# AICO: Backend System
For documentation on the frontend, please see: frontend/README.md

## Installation
If you have a GPU. Uncomment every `runtime: nvidia` line in `docker-compose.yml` and install
[nvidia-docker2](https://github.com/NVIDIA/nvidia-docker). If not, leave everything as it is.

Now run:
```bash
docker-compose build
```
Note that you may have to run this with sudo permissions, if you get an "Couldn't connect to Docker daemon" error

## Running
### Serving (via HTTP API) 
To use the application in its normal execution mode after training has been done, run:
```bash
docker-compose up web
```

### Training
To train the model run:
```bash
docker-compose up train
```
Note that you may have to run this with sudo permissions, if you get an "Couldn't connect to Docker daemon" error

In case you get an Access Denied error from elasticsearch, run:
```bash
mkdir -p database/data && sudo chmod -R 777 database
```

## Tests
Either from the root directory or within any /tests directory run:
```bash
pytest
```
if that does not work, try: `python -m pytest`

## Usage
When [running the API](#serving-via-http-api) send a HTTP request (GET or POST) to:

#### Full system
Endpoint:
```http request
127.0.0.1:8000
```

It should contain a  *JSON* body: 
```json
{ "query": "..." }
```
or an HTTP query parameter `?query=...`

##### End to End with Speech to Text
Send a `POST` HTTP request to `127.0.0.1:8000/voice` with a multipart/form-data body that has a single key value pair of `recording` and the full name of the audio file respectively.

###### Testing
Within a terminal in the directory of the audio file you wish to use, send a POST HTTP request like this:
```bash
curl -v -F 'recording=@path/to/fullAudioFilename' 127.0.0.1:8000/voice
```

#### Relate only
Endpoint:
```http request
127.0.0.1:8001
```
It should contain a  *JSON* body: 
```json
{ "query": "..." , "answers": ["...", "...", "..."]}
```
or HTTP query parameters: `?query=...&answers=...,...,...` (If there is a comma in an answer,
 the answer will be split into 2 answers, thus JSON body is preferable)
 
#### Search only
Endpoint:
```bash
127.0.0.1:8002
```

It should contain a  *JSON* body: 
```json
{ "query": "..." }
```
or an HTTP query parameter: `?query=...`

---
## Evaluation Script Usage
Run the evaluator.py script in the evaluate/evaluate folder.
```bash
python evaluator.py </path/to/input.json> </path/to/output.csv>
```

The script expects a dataset with the following format:
```json
{"query": "What's your name?", "answer": "My name is John.", "relevant": "yes"}
{"query": "What's your name?", "answer": "I'm ok.", "relevant": "no"}
...
```

Attribution
---

Repository icon made by <a href="https://www.flaticon.com/authors/dave-gandy" title="Dave Gandy">Dave Gandy</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
