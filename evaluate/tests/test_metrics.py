import pytest
from typing import List, Set, Tuple
from evaluate.metrics import *


@pytest.mark.parametrize("evaluate_tuples,strategy,expected", [
    ([(1, 1), (1, 1), (1, 1), (1, 1)], AmbiguousStrategy.ANY, (4, 0, 0, 0)),
    ([(1, 0.5), (1, 0.5), (1, 0.5), (1, 0.5)], AmbiguousStrategy.ANY, (4, 0, 0, 0)),
    ([(1, 0.5), (1, 0.5), (1, 0.5), (1, 0.5)], AmbiguousStrategy.MUST_PREDICT, (4, 0, 0, 0)),
    ([(1, 0.5), (1, 0.5), (1, 0.5), (1, 0.5)], AmbiguousStrategy.MUST_NOT_PREDICT, (0, 0, 4, 0)),
    ([(0, 0.5), (0, 0.5), (0, 0.5), (0, 0.5)], AmbiguousStrategy.ANY, (0, 0, 0, 4)),
    ([(0, 0), (0, 0), (0, 0), (0, 0)], AmbiguousStrategy.ANY, (0, 0, 0, 4)),
    ([(1, 0), (1, 0), (1, 0), (1, 0)], AmbiguousStrategy.ANY, (0, 0, 4, 0)),
    ([(0, 1), (0, 1), (0, 1), (0, 1)], AmbiguousStrategy.ANY, (0, 4, 0, 0)),
    ([(0, 1), (0, 1), (0, 1), (1, 0)], AmbiguousStrategy.ANY, (0, 3, 1, 0)),
])
def test_calculate_confusion_matrix_success(evaluate_tuples: List[Tuple[float, float]],
                                            strategy,
                                            expected: Tuple[int, int, int, int]):
    # Assign

    # Art
    result = calculate_confusion_matrix(evaluate_tuples, strategy)

    # Assert
    assert result == expected

@pytest.mark.parametrize("confusion_matrix,expected", [
    ((1, 0, 0, 0), 1),
    ((1, 0, 0, 1), 1),
    ((0, 1, 1, 0), 0),
    ((1, 0, 1, 0), 0.5),
    ((1, 1, 1, 0), 0.5),
    ((1, 1, 1, 1), 0.5)
])

def test_calculate_precision(confusion_matrix: Tuple[int, int, int, int], expected: int):
    # Assign

    # Art
    result = calculate_precision(confusion_matrix)

    # Assert
    assert result == expected


@pytest.mark.parametrize("confusion_matrix,expected", [
    ((1, 0, 0, 0), 1),
    ((1, 0, 0, 1), 1),
    ((0, 1, 1, 0), 0),
    ((1, 1, 0, 0), 0.5),
    ((1, 1, 0, 1), 0.5),
    ((1, 1, 1, 0), 0.5),
    ((1, 1, 1, 1), 0.5)
])
def test_calculate_recall(confusion_matrix: Tuple[int, int, int, int], expected: int):
    # Assign

    # Art
    result = calculate_recall(confusion_matrix)

    # Assert
    assert result == expected


@pytest.mark.parametrize("precision,recall,expected", [
    (1.0, 1.0, 1.0),
    (1.0, 0.5, 2 / 3),
    (0.5, 1.0, 2 / 3),
    (1.0, 0.25, 0.4),
    (0.25, 1.0, 0.4),
    (0.5, 0.25, 1 / 3),
    (0.25, 0.5, 1 / 3),
    (0.5, 0.5, 0.5),
    (0.25, 0.25, 0.25)
])
def test_calculate_f1_score(precision: float, recall: float, expected: float):
    # Assign

    # Art
    result = calculate_f1_score(precision, recall)

    # Assert
    assert result == expected


@pytest.mark.parametrize("top_n_predicted_and_actual_scores,expected", [
    ([(1, 1), (1, 1), (1, 1), (1, 1), (1, 1)], 0),
    ([(0, 0), (0, 0), (0, 0), (0, 0), (0, 0)], 0),
    ([(0, 1), (1, 0), (0, 1), (1, 0)], 1),
    ([(0.5, 1), (0, 1), (1, 0.5), (0, 0.5), (0.75, 0.5)], 0.55)
])
def test_calculate_query_mean_absolute_error(
        top_n_predicted_and_actual_scores: List[Tuple[float, float]],
        expected: float):
    # Assign

    # Art
    result = calculate_query_mean_absolute_error(top_n_predicted_and_actual_scores)

    # Assert
    assert result == expected


@pytest.mark.parametrize("top_n_predicted_and_actual_scores,expected", [
    ([(1, 1), (1, 1), (1, 1), (1, 1), (1, 1)], 1),
    ([(0, 0), (0, 0), (0, 0), (0, 0), (0, 0)], 0)
])
def test_calculate_query_average_precision(top_n_predicted_and_actual_scores: List[Tuple[float, float]],
                                           expected: float):
    # Assign

    # Art
    result = calculate_query_average_precision(top_n_predicted_and_actual_scores)

    # Assert
    assert result == expected


@pytest.mark.parametrize("top_n_predicted_and_actual_scores,expected", [
    ([(1, 1), (0, 0), (0, 0), (0, 0), (0, 0)], 1),
    ([(0, 1), (1, 0), (1, 0), (1, 0), (1, 0)], 1),
    ([(0, 0), (0, 1), (0, 0), (0, 0), (0, 0)], 0.5),
    ([(1, 0), (1, 1), (1, 0), (1, 0), (1, 0)], 0.5)
])
def test_calculate_query_reciprocal_rank(top_n_predicted_and_actual_scores: List[Tuple[float, float]],
                                         expected: float):
    # Assign

    # Art
    result = calculate_query_reciprocal_rank(top_n_predicted_and_actual_scores)

    # Assert
    assert result == expected


@pytest.mark.parametrize("top_n_predicted_and_actual_scores,expected", [
    ([(1, 1), (1, 1), (1, 1), (1, 1), (1, 1)], 1),
    ([(0, 0), (0, 0), (0, 0), (0, 0), (0, 0)], 0),
    ([(0, 0), (0, 0), (0, 0), (0, 0), (1, 1)], 0.12092199990360372),
    ([(0.5, 0.5), (1, 1), (0, 0), (0.25, 0.25), (0.125, 0.125)], 0.47137005689543515)
])
def test_calculate_normalised_dcg(top_n_predicted_and_actual_scores: List[Tuple[float, float]],
                                  expected: float):
    # Assign

    # Art
    result = calculate_query_normalised_dcg(top_n_predicted_and_actual_scores)

    # Assert
    assert result == expected
