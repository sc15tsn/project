import sys
from unittest import TestCase
from unittest.mock import mock_open
from unittest.mock import patch
from evaluate.evaluator import *


@patch("evaluate.evaluator.extract_evaluation_dataset")
@patch("evaluate.evaluator.fetch_answers")
@patch("evaluate.evaluator.np")
class EvaluatorTests(TestCase):

    def test_evaluate(self, mock_np, mock_fetch_answers, mock_extract_evaluation_dataset):
        # Assign
        queries_to_answers = {"What time is it?": {"It's 4pm.": 1, "I don't know.": 1}}
        mock_extract_evaluation_dataset.return_value = \
            queries_to_answers, len(set((r for _, rs in queries_to_answers.items() for r in rs)))
        mock_fetch_answers.return_value = queries_to_answers, queries_to_answers

        # Act
        with patch("evaluate.evaluator.open", mock_open()):
            result = evaluate("input.json", Hyperparams(0.5, 10, 20, None))

        # Assert
        assert len(result) == 8
        mock_extract_evaluation_dataset.assert_called_once()
        mock_fetch_answers.assert_called_once()
        assert len(mock_np.mean.mock_calls) % 8 == 0
        for call in mock_np.mean.calls:
            assert call.call_args_list[0] > 0.0
            assert call.call_args_list[0] < 1.0

    def test_write_results(self, mock_np, mock_fetch_answers, mock_extract_evaluation_dataset):
        # Assign
        metrics = Metrics(1, 0.5, 0.75, 0.5, 1, 0.5, 1, 0.5)

        lines = "search_recall,precision,recall,f1,mae,map,mrr,ndcg\n" \
            f"{metrics.search_recall}," \
            f"{metrics.precision}," \
            f"{metrics.recall}," \
            f"{metrics.f1}," \
            f"{metrics.mae}," \
            f"{metrics.map}," \
            f"{metrics.mrr}," \
            f"{metrics.ndcg}"

        # Act
        m_open = mock_open()
        with patch("evaluate.evaluator.open", m_open):
            with patch.object(sys, 'argv', ["", "", "results.test.csv"]):
                write_results('results.test.csv', metrics)

        # Assert
        m_open.assert_called_once()
        m_open.assert_called_with("results.test.csv", 'w', encoding='utf-8')
        m_open().write.assert_called_once_with(lines)
