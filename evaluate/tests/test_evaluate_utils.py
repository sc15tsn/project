from unittest import mock

import pytest
import responses
from evaluate.utils import *


@responses.activate
def test_fetch_answers_success():
    # Assign
    responses.add(responses.POST, 'http://localhost:8000/text',
                  json={
                      'answer_clusters': [
                          [
                              {"text": "test1", "score": 0.99},
                              {"text": "test2", "score": 0.97},
                          ],
                          [
                              {"text": "test3", "score": 0.98},
                          ]
                      ]
                  })

    responses.add(responses.POST, 'http://localhost:8002',
                  json={
                      'answers': [
                          "tes1t", "test2", "test3"
                      ]
                  })

    # Art
    fetch_answers(['test_query', 'test_query2'], Hyperparams(0.5, 10, 20, None))

    # Assert
    assert len(responses.calls) == 4


def test_extract_evaluation_dataset():
    # Assign
    read_data = '{"query": "What time is it?", "answer": "4pm", "relevance": "1"}\n' \
                '{"query": "What time is it?", "answer": "No.", "relevance": "0"}\n' \
                '{"query": "Who am I?", "answer": "a test", "relevance": "0.5"}'
    mock_open = mock.mock_open(read_data=read_data)

    # Art
    with mock.patch('evaluate.utils.open', mock_open):
        result = extract_evaluation_dataset("input.json")

    # Assert
    mock_open.assert_called_once()
    assert result[0] == {'What time is it?': {'4pm': 1, 'No.': 0}, 'Who am I?': {'a test': 0.5}}
    assert result[1] == 3


@pytest.mark.parametrize("predicted,actual_to_scores,expected", [
    (['a', 'b', 'c', 'd', 'e'], {'a': 1.0, 'b': 1.0, 'c': 1.0, 'd': 1.0, 'e': 1.0},
     [(1.0, 1.0), (1.0, 1.0), (1.0, 1.0), (1.0, 1.0), (1.0, 1.0)]),
    (['b', 'c', 'd', 'e', 'f'], {'a': 1.0, 'b': 1.0, 'c': 1.0, 'd': 1.0, 'e': 1.0},
     [(0.0, 1.0), (1.0, 1.0), (1.0, 1.0), (1.0, 1.0), (1.0, 1.0), (1.0, 0.0)]),
    (['d', 'e', 'f', 'g'], {'a': 1.0, 'b': 1.0, 'c': 1.0, 'd': 1.0, 'e': 1},
     [(0.0, 1.0), (0.0, 1.0), (0.0, 1.0), (1.0, 1.0), (1.0, 1.0), (1.0, 0.0), (1.0, 0.0)]),
    (['d', 'e', 'f', 'g'], {'a': 1.0, 'b': 1.0, 'c': 0.5, 'd': 1.0, 'e': 0},
     [(0.0, 1.0), (0.0, 1.0), (0.0, 0.5), (1.0, 1.0), (1.0, 0.0), (1.0, 0.0), (1.0, 0.0)]),
])
def test_generate_evaluation_tuples(predicted: List[str], actual_to_scores: Dict[str, float],
                                    expected: List[Tuple[float, float]]):
    # Assign

    # Art
    result = generate_evaluation_tuples(predicted, actual_to_scores)
    print("!")
    print(result)
    print(expected)
    # Assert
    assert result == expected
