"""This module contains functions implementing various evaluation metrics."""

from typing import List, Tuple, NamedTuple
from numpy import log2

Metrics = NamedTuple("Metrics", [("search_recall", float),
                                 ("precision", float),
                                 ("recall", float),
                                 ("f1", float),
                                 ("mae", float),
                                 ("map", float),
                                 ("mrr", float),
                                 ("ndcg", float)
                                 ])


class AmbiguousStrategy:
    """Strategy for handling ambiguities when calculating the confusion matrix"""
    ANY = 'any'
    MUST_PREDICT = 'must_predict'
    MUST_NOT_PREDICT = 'must_not_predict'


def calculate_confusion_matrix(evaluation_tuples: List[Tuple[float, float]],
                               strategy: AmbiguousStrategy = AmbiguousStrategy.ANY):
    """Calculates confusion matrix where 0.5 can be interpreted either way

    Args:
        evaluation_tuples: predicted, actual
        strategy: strategy on how to handle ambiguous values
    Returns:
        tp, fn, fp, tn
    """
    tp, fn, fp, tn = 0, 0, 0, 0

    for pred, act in evaluation_tuples:
        pred = round(pred)

        if act == 0.5:
            # Handle ambiguities according to strategy
            if pred == 1:
                if strategy == AmbiguousStrategy.MUST_NOT_PREDICT:
                    fp += 1
                else:
                    tp += 1
            elif pred == 0:
                if strategy == AmbiguousStrategy.MUST_PREDICT:
                    fn += 1
                else:
                    tn += 1
        else:
            # Handle everything else
            if pred == act == 1:
                tp += 1
            elif pred == act == 0:
                tn += 1
            if pred == 0 and act == 1:
                fn += 1
            if pred == 1 and act == 0:
                fp += 1

    return tp, fn, fp, tn


def calculate_precision(confusion_matrix: Tuple[int, int, int, int]) -> float:
    """Calculates precision (as generally known in information retrieval).

    Args:
        confusion_matrix: Confusion matrix (tuple ordering is TP, FN, FP, TN)

    Returns:
        The precision, as a floating-point number.
    """
    true_positives = confusion_matrix[0]
    false_positives = confusion_matrix[2]

    if true_positives == 0 and false_positives == 0:
        return 0
    else:
        return true_positives / (true_positives + false_positives)


def calculate_recall(confusion_matrix: Tuple[int, int, int, int]) -> float:
    """Calculates recall (as generally known in information retrieval).

    Args:
        confusion_matrix: the confusion matrix (tuple ordering is TP, FN, FP, TN)

    Returns:
        The recall, as a floating-point number.
    """
    true_positives = confusion_matrix[0]
    false_negatives = confusion_matrix[1]

    if true_positives == 0 and false_negatives == 0:
        return 0
    else:
        return true_positives / (true_positives + false_negatives)


def calculate_f1_score(precision: float, recall: float) -> float:
    """Calculates the F1 score.

    Args:
        precision: Precision.
        recall: Recall.

    Returns:
        A floating-point number representing the F1 score.
    """
    if precision == 0 and recall == 0:
        return 0
    else:
        return 2 * ((precision * recall) / (precision + recall))


def calculate_query_mean_absolute_error(top_n_predicted_and_actual_scores: List[Tuple[float, float]]) \
        -> float:
    """Calculates a query's mean absolute error.

    Args:
        top_n_predicted_and_actual_scores: a list of 2-tuples, with the first element being
            the predicted relevancy score and the second value being the actual relevancy score.

    Returns:
        a floating-point number representing the query's mean absolute error.
    """
    # If no answers were returned, we assumed that the mean absolute error is 1.
    if len(top_n_predicted_and_actual_scores) == 0:
        return 1

    intermediate_sum_qmae = 0.0

    for answer, score in top_n_predicted_and_actual_scores:
        predicted_relevancy_score = answer
        actual_relevancy_score = score

        intermediate_sum_qmae += abs(predicted_relevancy_score - actual_relevancy_score)

    query_mean_absolute_error = intermediate_sum_qmae / len(top_n_predicted_and_actual_scores)

    return query_mean_absolute_error


def calculate_query_average_precision(top_n_predicted_and_actual_scores: List[Tuple[float, float]]) -> float:
    """Calculates a query's mean average precision.

    Args:
        top_n_predicted_and_actual_scores: a list of 2-tuples, with the first element being
            the predicted relevancy score and the second value being the actual relevancy score.

    Returns:
        a floating-point number representing the query's mean average precision.
    """
    intermediate_sum_map = 0.0
    query_average_precision = 0.0
    relevant_answers_counter = 0

    for rank, (answer, score) in enumerate(top_n_predicted_and_actual_scores, 1):
        actual_relevancy_score = score

        # If the answer is relevant than calculate its precision
        if actual_relevancy_score:
            relevant_answers_counter += 1
            intermediate_sum_map += relevant_answers_counter / rank

    if relevant_answers_counter > 0:
        query_average_precision = intermediate_sum_map / relevant_answers_counter

    return query_average_precision


def calculate_query_reciprocal_rank(top_n_predicted_and_actual_scores: List[Tuple[float, float]]) -> float:
    """Calculate the query's reciprocal rank.

    Args:
        top_n_predicted_and_actual_scores: a list of 2-tuples, with the first element being
            the predicted relevancy score and the second value being the actual relevancy score.

    Returns:
        a floating-point number representing the query's reciprocal rank.
    """
    reciprocal_rank = 0.0

    for rank, (answer, score) in enumerate(top_n_predicted_and_actual_scores, 1):
        actual_relevancy_score = score

        # We calculate the query's reciprocal rank by considering the first relevant answer.
        # If the given answer is relevant, calculate the query's reciprocal rank,
        # using the rank of the answer we encountered.
        if actual_relevancy_score:
            reciprocal_rank = 1 / rank
            break

    return reciprocal_rank


def calculate_query_normalised_dcg(top_n_predicted_and_actual_scores: List[Tuple[float, float]]) -> float:
    """Calculates the normalised discounted cumulative gain (NDCG).

    Args:
        top_n_predicted_and_actual_scores: a list of 2-tuples containing the predicted
            relevancy scores and actual relevancy scores.

    Returns:
        a floating-point number representing the normalised discounted cumulative gain.
    """
    discounted_cumulative_gain = 0.0
    discounted_cumulative_gain_ideal = 0.0
    normalised_discounted_cumulative_gain = 0.0

    for rank, (answer, score) in enumerate(top_n_predicted_and_actual_scores, 1):
        actual_relevancy_score = score

        # Calculate discounted gain of the answer (DG for the first item is the actual relevancy score)
        discounted_gain = actual_relevancy_score if rank == 1 else actual_relevancy_score / log2(rank)
        discounted_gain_ideal = 1 if rank == 1 else 1 / log2(rank)

        # Add the above-calculated values to the sum for the query
        discounted_cumulative_gain += discounted_gain
        discounted_cumulative_gain_ideal += discounted_gain_ideal

    # Calculate the normalised discounted cumulative gain for this query/question
    # Note: DCG of ideal/ground truth has been calculated along the way,
    # this is where all returned top k answers
    # are of the highest relevancy
    if discounted_cumulative_gain_ideal > 0:
        normalised_discounted_cumulative_gain = discounted_cumulative_gain / discounted_cumulative_gain_ideal

    return normalised_discounted_cumulative_gain

    print(f"Predicted: {predicted}\nActual: {actual}")
    print(f"{true_positives} (TP), {false_negatives} (FN), {false_positives} (FP), {true_negatives} (TN)")

    return true_positives, false_negatives, false_positives, true_negatives
