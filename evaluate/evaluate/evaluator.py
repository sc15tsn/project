"""Runs the methods for calculating the metrics and writing out the results to a file."""
import argparse
import time
from typing import NamedTuple

import numpy as np

from evaluate.metrics import *
from evaluate.utils import fetch_answers, extract_evaluation_dataset, generate_evaluation_tuples, Hyperparams


def evaluate(eval_filepath: str,
             hyperparams: Hyperparams,
             ambiguous_strategy: AmbiguousStrategy = AmbiguousStrategy.ANY) -> Metrics:
    """Calculate metrics using pre-defined metric calculation methods (in metrics module).

    Args:
        eval_filepath: file path to the evaluation dataset.
        hyperparams: Hyper-parameters
        ambiguous_strategy: Strategy for handling ambiguities when calculating confusion
            matrix.

    Returns:
        a tuple containing all the metrics
    """
    start_time = time.time()

    queries_to_actual_answers_to_scores, dataset_size = extract_evaluation_dataset(eval_filepath)
    queries = queries_to_actual_answers_to_scores.keys()

    queries_to_found_answers, queries_to_predicted_answers = fetch_answers(queries, hyperparams)

    # Create lists to store metrics for each query - we will use this later to calculate average values.
    precision_list, recall_list, f1_list, qmae_list, qap_list, qrr_list, qndcg_list = [], [], [], [], [], [], []
    search_recall_list = []

    for query, predicted_answers in queries_to_predicted_answers.items():
        # Generate a list of (predicted_score, actual_score) tuples
        print(f"Query: {query}")

        # Generate a list of (predicted_score, actual_score) tuples
        evaluation_tuples = generate_evaluation_tuples(predicted_answers,
                                                       queries_to_actual_answers_to_scores[query])

        actual = set(queries_to_actual_answers_to_scores[query])
        actual_in_search_results = actual.intersection(queries_to_found_answers[query])

        # Calculate the percentage of actual answers returned by Elasticsearch
        search_recall_list += (len(actual_in_search_results) / len(actual),)

        # Calculate and store metrics for the query
        confusion_matrix = calculate_confusion_matrix(evaluation_tuples, ambiguous_strategy)
        precision = calculate_precision(confusion_matrix)
        recall = calculate_recall(confusion_matrix)

        precision_list += (precision,)
        recall_list += (recall,)
        f1_list += (calculate_f1_score(precision, recall),)
        qmae_list += (calculate_query_mean_absolute_error(evaluation_tuples),)
        qap_list += (calculate_query_average_precision(evaluation_tuples),)
        qrr_list += (calculate_query_reciprocal_rank(evaluation_tuples),)
        qndcg_list += (calculate_query_normalised_dcg(evaluation_tuples),)

    # Show elapsed time
    elapsed_time = time.time() - start_time

    metrics = Metrics(
        float(np.mean(search_recall_list)),
        float(np.mean(precision_list)),
        float(np.mean(recall_list)),
        float(np.mean(f1_list)),
        float(np.mean(qmae_list)),
        float(np.mean(qap_list)),
        float(np.mean(qrr_list)),
        float(np.mean(qndcg_list))
    )

    print(f"\n-- Metrics --\n"
          f"Search recall: {metrics.search_recall}\n"
          f"Precision: {metrics.precision}\n"
          f"Recall: {metrics.recall}\n"
          f"F1: {metrics.f1}\n"
          f"MAE: {metrics.mae}\n"
          f"MAP: {metrics.map}\n"
          f"MRR: {metrics.mrr}\n"
          f"NDCG: {metrics.ndcg}\n")

    print(f"It took {elapsed_time:.2f} seconds to evaluate.")

    return metrics


def write_results(output_filepath,
                  metrics: Metrics) -> None:
    """
    Writes the evaluation results to a CSV file.

    Args:
        output_filepath: Output filepath
        metrics: Tuple containing the evaluation results
    """
    with open(output_filepath, 'w', encoding='utf-8') as out:
        out.write("search_recall,precision,recall,f1,mae,map,mrr,ndcg\n"
                  f"{metrics.search_recall},"
                  f"{metrics.precision},"
                  f"{metrics.recall},"
                  f"{metrics.f1},"
                  f"{metrics.mae},"
                  f"{metrics.map},"
                  f"{metrics.mrr},"
                  f"{metrics.ndcg}"
                  )


def serialize_hyperparams(hyperparams: Hyperparams):
    return f'T-{hyperparams.threshold}_' \
        f'MAR-{hyperparams.max_answers_relate}_' \
        f'MAS-{hyperparams.max_answers_search}_' \
        f'TSF-{hyperparams.search_text_fuzziness}'


def main():
    parser = argparse.ArgumentParser(
        description="Evaluate AICO performance.")
    parser.add_argument("eval_file", type=str, default='eval.tsv',
                        help="path to the TSV/JSON input evaluation file containing with "
                             "'query', 'answer', 'relevant' keys/order.")
    parser.add_argument("output_file", type=str, default='results.csv',
                        help="path to the JSON output file containing metrics.")
    parser.add_argument("--threshold", type=float, default=None,
                        help="Threshold relevancy score.")
    parser.add_argument("--max_answers_relate", type=int, default=None, required=False,
                        help="Maximum answers retrieved by relate.")
    parser.add_argument("--max_answers_search", type=int, default=None, required=False,
                        help="Maximum answers retrieved by search.")
    parser.add_argument("--search_text_fuzziness", type=int, default=None, required=False,
                        help="Fuzziness applied to the answer search.")
    parser.add_argument("--include_hyperparams_in_filename", type=bool, default=True, required=False,
                        help='Whether to include hyperparameters automatically in output filename.')
    parser.add_argument("--strategy", type=str, default='any',
                        help="ambiguity strategy ('any','must_predict','must_not_predict') (default: 'any)")
    args = parser.parse_args()

    hyperparams = Hyperparams(threshold=args.threshold,
                              max_answers_relate=args.max_answers_relate,
                              max_answers_search=args.max_answers_search,
                              search_text_fuzziness=args.search_text_fuzziness)

    # Include hyperparams in filename if needed
    if args.include_hyperparams_in_filename:
        split_fn = args.output_file.split(".")
        out_filename = f'{split_fn[0]}_{serialize_hyperparams(hyperparams)}.{args.strategy}.{".".join(split_fn[1:])}'
    else:
        out_filename = args.output_file

    print(f"Eval file: {args.eval_file}")
    print(f"Output file: {out_filename}")
    print(f"Threshold: {hyperparams.threshold}")
    print(f"Max answers (relate): {hyperparams.max_answers_relate}")
    print(f"Max answers (search): {hyperparams.max_answers_search}")
    print(f"Search text fuzziness: {hyperparams.search_text_fuzziness}")
    print(f"Ambiguity strategy: {args.strategy}")

    # Calculate the metrics
    metrics = evaluate(args.eval_file,
                       hyperparams=hyperparams,
                       ambiguous_strategy=args.strategy)

    # Write results out to a file
    write_results(out_filename, metrics)


if __name__ == '__main__':
    main()
