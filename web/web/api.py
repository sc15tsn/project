"""HTTP API interface that is exposed to the web"""
from cgi import FieldStorage

import falcon
import requests
from falcon_cors import CORS
from falcon_multipart.middleware import MultipartMiddleware
from typing import List, Any
from ibm_cloud_sdk_core.api_exception import ApiException as IbmCloudApiException
from ibm_watson.speech_to_text_v1 import SpeechToTextV1
from ibm_watson.text_to_speech_v1 import TextToSpeechV1

from web.constants import HTTP_QUERY_PARAMS_ENABLED, RELATE_URL, SEARCH_URL, STT_API_KEY, STT_API_URL, \
    TTS_API_KEY, \
    TTS_API_URL, ALLOW_ORIGINS_LIST, ALLOW_HEADERS_LIST, ALLOW_METHODS_LIST
from web import log

LOGGER = log.get_logger(__name__)


class WebResource:
    """Resource object for Web API"""

    def on_any(self, req: falcon.Request, resp: falcon.Response) -> None:
        """Base method for connecting components together"""
        raise NotImplementedError()

    def on_get(self, req: falcon.Request, resp: falcon.Response) -> None:
        """Redirects to on_all"""
        self.on_any(req, resp)

    def on_post(self, req: falcon.Request, resp: falcon.Response) -> None:
        """Redirects to on_all"""
        self.on_any(req, resp)

    @staticmethod
    def search(query: str,
               sentiment: str,
               max_answers: int = None,
               search_text_fuzziness: int = None) -> List[str]:
        """Performs a search for answers via Elasticsearch.

        Args:
            query: Query for which to search for answers.
            sentiment: Tone of prospective responses.
            max_answers: Maximum number of answers to retrieve.
            search_text_fuzziness: Fuzziness setting for the search text.

        Returns:
            A list of answers as returned by Elasticsearch.
        """
        req_json = {'query': query}

        if sentiment:
            req_json['sentiment'] = sentiment

        if max_answers:
            req_json['max_answers'] = max_answers

        if search_text_fuzziness:
            req_json['search_text_fuzziness'] = search_text_fuzziness

        LOGGER.debug("Searching answers for query '%s'... (search_text_fuzziness: %d, max_answers: %d)",
                     query, search_text_fuzziness, max_answers)
        response = requests.post(SEARCH_URL, json=req_json)
        answers = response.json()['answers']

        LOGGER.debug("Answers: %s", str(answers))
        return answers

    @staticmethod
    def relate(query: str,
               answers: List[str],
               threshold: float = None,
               max_answers: int = None,
               cluster_eps: float = None) -> List[str]:
        """Relates the answers returned by the search module.

        Args:
            query: User's query.
            answers: Potential answers.
            threshold: Threshold value for relevancy of answers.
            max_answers: Number of top answers to retrieve.
            cluster_eps: Epsilon value for clustering (Look up DBSCAN).

        Returns:
            a list of related answers
        """
        LOGGER.info(
            f"Relating '{query}' with {len(answers)} answers"
            f" (threshold={threshold}, max_answers={max_answers}).")

        req_json = {'query': query,
                    'answers': answers}

        if threshold:
            req_json['threshold'] = threshold

        if max_answers:
            req_json['max_answers'] = max_answers

        if cluster_eps:
            req_json['cluster_eps'] = cluster_eps

        response = requests.post(RELATE_URL, json=req_json)
        related_answers = response.json()['answer_clusters']

        LOGGER.debug("Related answers: %s", str(related_answers))
        return related_answers

    @staticmethod
    def get_param(param_name: str,
                  req: falcon.Request,
                  required: bool = True,
                  allow_query_param: bool = HTTP_QUERY_PARAMS_ENABLED) -> Any:
        """Get the parameter from either JSON body or HTTP Query params (if enabled)

        Args:
            param_name: Parameter name.
            req: falcon.Request object where the param_name will be retrieved from.
            resp: falcon.Response object.
            required: True if it is required to retrieve the parameter.
                If it is required and parameter is not specified, it will
                change the resp with HTTP_BAD_REQUEST and an appropriate message.
            allow_query_param: True if HTTP Query parameters should be allowed.

        Returns:
            Parameter if found in the request, otherwise None.
        """
        try:
            # Get the parameter from JSON body
            param = req.media.get(param_name)
        except (AttributeError, falcon.errors.HTTPBadRequest, falcon.errors.HTTPUnsupportedMediaType):
            # Get the parameter from HTTP Query if not found in JSON body
            param = req.get_param(param_name) if allow_query_param else None

        if param is None and required:
            raise falcon.HTTPBadRequest(description="You have to specify '%s' parameter." % param_name)

        return param


class TextQueryToAnswersResource(WebResource):
    """Resource object for Web API"""

    def on_any(self, req: falcon.Request, resp: falcon.Response) -> None:
        """Connects speech_to_text, search, relate components together.

        Calls the search to get the potential answers,
        then calls relate on them to relate them.

        Args:
            req: falcon.Request with `recording` parameter specified.
            resp: falcon.Response

        Notes:
            if successful the response body contains `answer_clusters`.
        """
        query = self.get_param('query', req)
        sentiment = self.get_param('sentiment', req, required=False)
        threshold = self.get_param("threshold", req, required=False)
        max_answers_relate = self.get_param("max_answers_relate", req, required=False)

        search_text_fuzziness = self.get_param(
            'search_text_fuzziness', req, allow_query_param=True, required=False)
        max_answers_search = self.get_param("max_answers_search", req, required=False)
        cluster_eps = self.get_param("cluster_eps", req, required=False)

        threshold = float(threshold) if threshold else None
        max_answers_relate = int(max_answers_relate) if max_answers_relate else None
        cluster_eps = float(cluster_eps) if cluster_eps else None

        max_answers_search = int(max_answers_search) if max_answers_search else None
        search_text_fuzziness = int(search_text_fuzziness) if search_text_fuzziness else None

        answers = self.search(query, sentiment,
                              max_answers=max_answers_search,
                              search_text_fuzziness=search_text_fuzziness)
        answer_clusters = self.relate(query, answers,
                                      threshold=threshold,
                                      max_answers=max_answers_relate,
                                      cluster_eps=cluster_eps)

        # Create a JSON representation of the resource
        resp.media = {'answer_clusters': answer_clusters}


class VoiceQueryToAnswersResource(WebResource):
    """Resource object for Web API that uses speech-to-text to get the query."""

    def __init__(self):
        self.speech_to_text_service = SpeechToTextV1(iam_apikey=STT_API_KEY, url=STT_API_URL)
        super().__init__()

    def on_any(self, req: falcon.Request, resp: falcon.Response) -> None:
        """Connects speech_to_text, search, relate components together.

        Calls the speech_to_text to convert the voice into text,
        passes the text to search to get the potential answers,
        then calls relate on them to relate them.

        Args:
            req: falcon.Request with `recording` parameter specified.
            resp: falcon.Response

        Notes:
            if successful the response body contains `answer_clusters`.
        """
        recording = self.get_param('recording', req, allow_query_param=True)
        sentiment = self.get_param('sentiment', req, allow_query_param=True, required=False)

        max_answers_search = self.get_param('max_answers_search', req, allow_query_param=True, required=False)
        search_text_fuzziness = self.get_param(
            'search_text_fuzziness', req, allow_query_param=True, required=False)

        max_answers_relate = self.get_param('max_answers_relate', req, allow_query_param=True, required=False)
        threshold = self.get_param('threshold', req, allow_query_param=True, required=False)
        cluster_eps = self.get_param('cluster_eps', req, allow_query_param=True, required=False)

        threshold = float(threshold) if threshold else None
        max_answers_relate = int(max_answers_relate) if max_answers_relate else None
        cluster_eps = float(cluster_eps) if cluster_eps else None

        max_answers_search = int(max_answers_search) if max_answers_search else None
        search_text_fuzziness = int(search_text_fuzziness) if search_text_fuzziness else None

        # Make sure it is a file.
        if not isinstance(recording, FieldStorage):
            raise falcon.HTTPBadRequest(
                description="You need to send a file recording of type multipart/form-data.")

        # Get the query.
        try:
            query = self.speech_to_text(recording)
        except ConnectionError as e:
            LOGGER.error(f"ERROR: Unable to connect to IBM Watson STT API!")
            raise falcon.HTTPServiceUnavailable(description="External API is unreachable")
        except IbmCloudApiException as e:
            LOGGER.error(f"ERROR: {e.message}")
            if e.code < 500:
                raise falcon.HTTPBadRequest(description=e.message)
            else:
                raise falcon.HTTPServiceUnavailable(description=e.message)
        except IndexError:
            LOGGER.error(f"ERROR: IndexError. No speech was detected.")
            raise falcon.HTTPBadRequest("No speech was detected.")

        # Search answers and relate them
        answers = self.search(query, sentiment,
                              max_answers=max_answers_search,
                              search_text_fuzziness=search_text_fuzziness)
        answer_clusters = self.relate(query, answers,
                                      threshold=threshold,
                                      max_answers=max_answers_relate,
                                      cluster_eps=cluster_eps)

        # Create a JSON representation of the resource
        resp.media = {'answer_clusters': answer_clusters}

    def speech_to_text(self, recording: FieldStorage) -> str:
        """Transforms a speech to text, using an external IBM Watson API.

        Args:
            recording (FieldStorage): Audio recording with a speech.
        Raises:
            IndexError: When no speech was detected.
            IBMCloudApiException: When IBM Cloud API raises an exception.
        """
        file_format = recording.filename.split(".")[-1]

        query = self.speech_to_text_service.recognize(
            audio=recording.file.read(),
            content_type=f"audio/{file_format}",
            model="en-GB_BroadbandModel"
        ).get_result()["results"][0]["alternatives"][0]["transcript"]

        LOGGER.debug("Query: %s", str(query))
        return query


class TextToSpeechResource(WebResource):
    """Resource object for Web API that uses text-to-speech to get synthesized audio."""

    def __init__(self):
        self.text_to_speech_service = TextToSpeechV1(iam_apikey=TTS_API_KEY, url=TTS_API_URL)
        super().__init__()

    def on_any(self, req: falcon.Request, resp: falcon.Response) -> None:
        """Calls text to speech external API to convert text into voice.

        Args:
            req: falcon.Request with `text` parameter specified
            resp: falcon.Response with byte stream of audio in the body of type audio/ogg

        Notes:
            if successful the response data contains the synthesized speech as a stream bytes.
        """
        text = self.get_param("text", req, allow_query_param=True)

        # Get the synthesized response audio
        try:
            synthesized_speech = self.text_to_speech(text)
        except IbmCloudApiException as e:
            LOGGER.error(f"ERROR: {e.message}")
            if e.code < 500:
                raise falcon.HTTPBadRequest(description=e.message)
            else:
                raise falcon.HTTPServiceUnavailable(description=e.message)

        resp.body = synthesized_speech
        resp.content_type = "audio/ogg;codecs=opus"
        resp.status = falcon.HTTP_OK

    def text_to_speech(self, text: str) -> bytes:
        """Transforms some text to speech, using an external IBM Watson API

        Args:
            text (str): Response text to be converted to synthesized speech
        Raises:
            IBMCloudApiException: When IBM Cloud API raises an exception.
        Returns:
            Stream of bytes representing the audio (audio of type audio/ogg)
        """
        synthesized_speech = self.text_to_speech_service.synthesize(
            text=text,
            voice="en-GB_KateVoice"
        ).get_result().content

        LOGGER.debug(f"Synthesized speech generated.")
        return synthesized_speech

    def on_get(self, req: falcon.Request, resp: falcon.Response) -> None:
        return self.on_any(req, resp)

    def on_post(self, req: falcon.Request, resp: falcon.Response) -> None:
        return self.on_any(req, resp)


def _generic_error_handler(ex, *_):
    """A catch-all error handler for the API.

    Args:
        ex: the exception that was originally thrown
    """
    if isinstance(ex, falcon.HTTPError):
        raise ex
    else:
        LOGGER.exception(ex)
        raise falcon.HTTPInternalServerError(description="An unforeseen error has occurred.")


def create() -> falcon.API:
    """Initialises the API.

    Returns:
        falcon.API
    """
    # Use falcon-cors middleware to support CORS headers
    # Use multipart middleware to enable handling of files as any other parameter
    cors_middleware = CORS(allow_origins_list=ALLOW_ORIGINS_LIST,
                           allow_headers_list=ALLOW_HEADERS_LIST,
                           allow_methods_list=ALLOW_METHODS_LIST).middleware
    multipart_middleware = MultipartMiddleware()

    api = falcon.API(middleware=[cors_middleware, multipart_middleware])

    text_res = TextQueryToAnswersResource()
    voice_res = VoiceQueryToAnswersResource()
    tts_res = TextToSpeechResource()

    api.add_route('/text', text_res)
    api.add_route('/voice', voice_res)
    api.add_route('/tts', tts_res)
    api.add_error_handler(Exception, _generic_error_handler)

    LOGGER.info("Web API is ready.")
    return api


WEB_API = create()
