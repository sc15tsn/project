from unittest.mock import patch, Mock
from falcon import status_codes, HTTPBadRequest
from pytest import raises
import requests_mock

from web import api
from web.api import WebResource
from web.constants import *


@patch("web.api.SpeechToTextV1")
def test_speechtotextv1_object_created_upon_initialisation(mock_SpeechToTextV1):
    # Act
    api.VoiceQueryToAnswersResource()

    # Assert
    assert mock_SpeechToTextV1.called


@patch("falcon.Request")
def test_if_request_extracted_with_media_get_param_not_called(mock_Request):
    # Assign

    # Mock return values of mocks
    mock_Request.media.get.return_value = "test"
    WebResource.get_param("my_param", mock_Request)

    # Assert
    mock_Request.media.get.assert_called()
    mock_Request.get_param.assert_not_called()


@patch("falcon.Request")
def test_if_request_extracted_without_media_but_query_param_disallowed_get_param_not_called(mock_Request):
    # Assign

    # Mock return values of mocks
    WebResource.get_param("my_param", mock_Request)

    # Assert
    mock_Request.media.get.assert_called()
    mock_Request.get_param.assert_not_called()


@patch("falcon.Request")
def test_if_request_extracted_without_media_and_query_param_allowed_get_param_is_called(mock_Request):
    # Assign

    # Mock return values of mocks
    mock_Request.media.get.side_effect = HTTPBadRequest("test")
    WebResource.get_param("my_param", mock_Request, allow_query_param=True)

    # Assert
    mock_Request.media.get.assert_called()
    mock_Request.get_param.assert_called()


@patch("falcon.Request")
@patch("falcon.Response")
def test_if_recording_empty_get_bad_request_response(mock_Response, mock_Request):
    # Assign
    wvr = api.VoiceQueryToAnswersResource()

    # Act
    mock_Request.get_param.return_value = ""
    with raises(HTTPBadRequest):
        wvr.on_any(mock_Request, mock_Response)


@requests_mock.Mocker(kw='requests_mocker')
def test_handle_search_request_returns_search_api_call_result(**kwargs):
    # Assign
    web_resource = api.WebResource()
    query = 'query'
    expected_answers = ['answer', 'another answer', 'third answer']

    # Mock response from request to search API
    requests_mocker = kwargs['requests_mocker']
    requests_mocker.register_uri('POST', SEARCH_URL, json={
        "answers": ['answer', 'another answer', 'third answer']
    })

    # Act
    result = web_resource.search(query, None)

    # Assert
    assert result == expected_answers


@requests_mock.Mocker(kw='requests_mocker')
def test_handle_relate_request_returns_relate_api_call_result(**kwargs):
    # Assign
    web_resource = api.WebResource()
    answers = ['answer', 'another answer', 'third answer']
    num_responses = 100
    expected_related_answers = [
        [
            {"text": "test1", "score": "0.99"},
        ], [
            {"text": "test1", "score": "0.98"},
            {"text": "test1", "score": "0.97"}
        ]
    ]

    # Mock response from request to relate API
    requests_mocker = kwargs['requests_mocker']
    requests_mocker.register_uri('POST', RELATE_URL, json={
        "answer_clusters": expected_related_answers
    })

    # Act
    result = web_resource.relate('query', answers, num_responses)

    # Assert
    assert result == expected_related_answers


@patch("web.api.TextToSpeechV1")
def test_texttospeechv1_object_created_upon_initialisation(mock_TextToSpeechV1):
    # Act
    api.TextToSpeechResource()

    # Assert
    assert mock_TextToSpeechV1.called


@patch("web.api.TextToSpeechV1")
def test_text_to_speech_calls_synthesize_method_once(mock_TextToSpeechV1):
    # Assign
    web_speech_resource = api.TextToSpeechResource()

    # Act
    web_speech_resource.text_to_speech("make this text into some speech")

    # Assert
    mock_TextToSpeechV1.return_value.synthesize.assert_called_once()
