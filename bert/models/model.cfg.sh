#!/bin/bash
USE_FINETUNED_MODEL=false

PRETRAINED_MODEL_NAME="uncased_L-12_H-768_A-12"
PRETRAINED_MODEL_DIR="/app/models/pretrained/${PRETRAINED_MODEL_NAME}"

FINETUNED_MODEL_NAME="bert-base-seq64-maxchars100"
FINETUNED_MODEL_DIR="/app/models/finetuned/${FINETUNED_MODEL_NAME}"

# Checkpoint number or 'latest'
FINETUNED_MODEL_CHECKPOINT="latest"