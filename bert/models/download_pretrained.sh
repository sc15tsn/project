#!/bin/bash
. /app/models/model.cfg.sh  # Load the config file

# Verify that pretrained model exists. If not, download it
if ! [[ -s ${PRETRAINED_MODEL_DIR}/bert_config.json && \
    -s ${PRETRAINED_MODEL_DIR}/bert_model.ckpt.data-00000-of-00001 && \
    -s ${PRETRAINED_MODEL_DIR}/bert_model.ckpt.index && \
    -s ${PRETRAINED_MODEL_DIR}/bert_model.ckpt.meta && \
    -s ${PRETRAINED_MODEL_DIR}/vocab.txt ]];
then
    echo -e "Could not find \e[33m${PRETRAINED_MODEL_NAME}\e[0m pre-trained model files. Downloading..."
    mkdir -p models/pretrained
    cd models/pretrained
    echo "Download complete. Unzipping..."
    apt-get -y install wget unzip
    wget https://storage.googleapis.com/bert_models/2018_10_18/${PRETRAINED_MODEL_NAME}.zip -O model.zip
    unzip -o model.zip
    rm model.zip
    cd ../..
    echo "Done"
fi