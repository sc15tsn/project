import {BACKEND_URL} from "../constants";
import logger from "../Logger";

export const playSelectedAnswerAudioAction = (selectedAnswer) => {
    return new Promise((resolve, reject) => {
        const encodedText = encodeURI(selectedAnswer);
        const audio = new Audio(`${BACKEND_URL}/tts?text=${encodedText}`);

        audio.onerror = (error) => {
            logger.logError(`Audio Stream/Playback Error, error=${JSON.stringify(error)}`,
                "actions.playSelectedAnswerAudioAction");

            reject(error);
        };

        audio.onended = () => {
            logger.logInfo("Audio Playback Completed.",
                "actions.playSelectedAnswerAudioAction");

            resolve();
        };

        logger.logInfo("Starting audio stream.", "actions.playSelectedAnswerAudioAction");

        audio.play()
            .catch((error) => {
                logger.logError(`Couldn't start audio stream/playback, error=${JSON.stringify(error)}`,
                    "actions.playSelectedAnswerAudioAction");
            })
            .then(() => {
                logger.logInfo("Audio Playback Started.",
                    "actions.playSelectedAnswerAudioAction");
            });
    });
};