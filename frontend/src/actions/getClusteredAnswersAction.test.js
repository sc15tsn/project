import {getClusteredAnswersAction} from './getClusteredAnswersAction';
import {GET_ANSWER_CLUSTERS} from "./types";
import {BACKEND_URL} from "../constants";

describe('getClusteredAnswersAction', () => {
    it('triggers fetch answers and get answers redux action', (done) => {
        const answers = [['answer 1', 'answer 2'], ['answer 3']];

        // Create a mock dispatcher
        const dispatchSpy = jest.fn();

        // mock out the fetch call
        jest.spyOn(global, "fetch").mockImplementation(() => Promise.resolve({
            ok: true,
            json: () => Promise.resolve({
                'answer_clusters': [
                    [
                        {"text": "answer 1", "score": "0.99"},
                        {"text": "answer 2", "score": "0.97"},
                    ],
                    [
                        {"text": "answer 3", "score": "0.98"}
                    ]
                ]
            })
        }));

        // Call the action
        const recordedBlob = {
            blob: new Blob(["blah"]),
            options: {
                mimeType: "audio/webm"
            }
        };

        const expectedRequestData = new FormData();
        expectedRequestData.append("recording", new Blob(), "queryAudio.webm");
        expectedRequestData.append("sentiment", "neu");

        getClusteredAnswersAction(recordedBlob, "meh")(dispatchSpy)
            .then(() => {
                expect(global.fetch).toHaveBeenCalledWith(`${BACKEND_URL}/voice`,
                    { method: 'POST', headers: {}, body: expectedRequestData });
                expect(dispatchSpy).toHaveBeenCalledWith({type: GET_ANSWER_CLUSTERS, payload: answers});
                done();
            })
            .catch(done.fail)
    });
});


