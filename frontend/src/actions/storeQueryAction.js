import {STORE_QUERY} from "./types";

export const storeQueryAction = (payload) => (dispatch) => {
    return new Promise((resolve) => {
        // This triggers the STORE_QUERY action in the query reducer
        dispatch({
            type: STORE_QUERY,
            payload: payload
        });

        resolve();
    });
};