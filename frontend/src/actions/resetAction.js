import {RESET_ALL} from "./types";

export const resetAction = () => (dispatch) => {
    return new Promise((resolve) => {
        // This triggers the RESET_ALL action in the query and answers reducers, with the end-result being that any
        // state variables under their control are cleared.
        dispatch({
            type: RESET_ALL
        });

        resolve();
    });
};