import {resetAction} from './resetAction';
import {RESET_ALL} from "./types";

describe('resetAction', () => {
    it('triggers reset redux action', () => {
        // Create a mock dispatcher
        const dispatchSpy = jest.fn();

        // Call the action
        resetAction()(dispatchSpy);

        // Assert that the dispatcher has been called
        expect(dispatchSpy).toHaveBeenCalled();
        expect(dispatchSpy).toHaveBeenCalledWith({ type: RESET_ALL });
    });
});