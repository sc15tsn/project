import debug from 'debug';

const BASE = 'aico-frontend';
const COLOURS = {
    info: 'green',
    error: 'red'
};

class Logger {
    constructor() {
        const logInfo = debug(`${BASE}:trace`);
        logInfo.color = COLOURS['info'];

        const logError = debug(`${BASE}:trace`);
        logError.color = COLOURS['error'];

        this.logHandlers = {
            info: logInfo,
            error: logError
        };
    }

    getLogHandler = (level) =>
        this.logHandlers[level];

    generateMessage = (level, message, source) =>
        source ?
            this.getLogHandler(level)(source, message)
            : this.getLogHandler(level)(message);

    logInfo = (message, source) =>
        this.generateMessage('info', message, source);

    logError = (message, source) =>
        this.generateMessage('error', message, source);
}

export default new Logger();