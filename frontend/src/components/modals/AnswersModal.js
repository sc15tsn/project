import React, {Component} from 'react';
import {MDBModal, MDBModalBody} from "mdbreact";
import PropTypes from "prop-types";
import ListView from "../views/ListView";

export default class AnswersModal extends Component {
    static propTypes = {
        isOpen: PropTypes.bool.isRequired,
        onClick: PropTypes.func.isRequired,
        toggle: PropTypes.func.isRequired,
        items: PropTypes.array.isRequired
    };

    render = () =>
        <MDBModal isOpen={this.props.isOpen} toggle={this.props.toggle} centered>
            <MDBModalBody>
                <ListView onClick={this.props.onClick} items={this.props.items} />
            </MDBModalBody>
        </MDBModal>;
}