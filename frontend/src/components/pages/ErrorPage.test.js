import React from 'react';
import {MDBAlert, MDBBtn} from "mdbreact";
import ErrorPage from "./ErrorPage";
import {createMemoryHistory} from "history";

describe('ErrorPage', () => {
    let errorPage, testHistory;

    beforeEach(() => {
        testHistory = createMemoryHistory();
        testHistory.push({
            pathname: '/error'
        });

        errorPage = mount(<ErrorPage history={testHistory} />);
    });

    it("should render error message", () => {
        expect(errorPage.find(MDBAlert).length).toBe(1);
    });

    it("should render Go Back & Retry button", () =>{
        const btn = errorPage.find(MDBBtn);

        expect(btn.length).toBe(1);
        expect(btn.text()).toEqual(" Go Back & Retry");
    });

    it("go back button on click should change location to previous page", (done) => {
        // Assert initial history state
        expect(testHistory.location.pathname).toEqual("/error");
        expect(testHistory.entries.length).toBe(2);
        expect(testHistory.entries[0].pathname).toEqual("/");
        expect(testHistory.entries[1].pathname).toEqual("/error");

        // Simulate click
        errorPage
            .find(MDBBtn)
            .simulate('click');

        // Assert resulting history state
        setTimeout(() => {
            expect(testHistory.location.pathname).toEqual("/");
            expect(testHistory.entries.length).toBe(2);
            expect(testHistory.entries[0].pathname).toEqual("/");
            expect(testHistory.entries[1].pathname).toEqual("/error");
            done();
        }, 2000);
    });
});