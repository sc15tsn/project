import React from 'react';
import {Provider} from 'react-redux';
import {applyMiddleware, createStore} from "redux";
import PropTypes from 'prop-types';
import rootReducer from "../../reducers";
import thunk from "redux-thunk";
import {MDBBtn, MDBInput, MDBRow} from "mdbreact";
import TextHomePage from "./TextHomePage";
import {createMemoryHistory} from "history";

describe('TextHomepage', () => {
    let home, wrapper, store;

    beforeEach(() => {
        store = createStore(
            rootReducer,
            applyMiddleware(thunk)
        );

        const options = {
            childContextTypes: {
                query: PropTypes.object.isRequired
            }
        };

        wrapper = mount(<Provider store={store}><TextHomePage /></Provider>, options);
        home = wrapper.find('TextHomePage');
    });

    it("should render text field", () => {
        expect(home.find(MDBInput).length).toBe(1);
    });

    it("should render next button", () => {
        expect(home.find(MDBBtn).length).toBe(1);
    });
});