import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import {MDBRow} from "mdbreact";
import {ReactMic} from 'react-mic'
import RecordBtn from '../buttons/RecordBtn';
import {storeQueryAction} from '../../actions/storeQueryAction.js'

/**
 * Home page component
 */
class HomePage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isRecording: false
        };

        this.onStop = this.onStop.bind(this);
        this.onClick = this.onClick.bind(this);
    }

    onStop = (recordedBlob) =>
        this.props
            .storeQueryAction(recordedBlob)
            .then(() => {
                this.props.history.push({
                    pathname: '/sentiment'
                });
            })
            .catch(() => {
                this.props.history.push({
                    pathname: '/error'
                });
            });

    onClick = () =>
        this.setState(state => ({
            isRecording: !state.isRecording
        }));

    render = () =>
        <>
            <MDBRow className="row">
                <ReactMic
                    record={this.state.isRecording}
                    className="sound-wave"
                    onStop={this.onStop}
                    strokeColor="#000000"
                    backgroundColor="#66ccff"/>
            </MDBRow>
            <MDBRow className="row" center>
                <RecordBtn isRecording={this.state.isRecording} onClick={this.onClick}/>
            </MDBRow>
        </>;
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators({storeQueryAction}, dispatch);

export default connect(null, mapDispatchToProps)(HomePage);