import React from 'react';
import {Provider} from 'react-redux';
import {applyMiddleware, createStore} from "redux";
import PropTypes from 'prop-types';
import rootReducer from "../../reducers";
import thunk from "redux-thunk";
import {createMemoryHistory} from "history";
import {MDBBtn, MDBListGroup, MDBListGroupItem} from "mdbreact";
import {GET_ANSWER_CLUSTERS, STORE_QUERY} from "../../actions/types";
import AnswersPage from "./AnswersPage";
import {playSelectedAnswerAudioAction} from "../../actions/playSelectedAnswerAudioAction";
import AnswerClusterSelectionView from "../views/AnswerClusterSelectionView";

// mock out the fetch call
jest.mock('../../actions/playSelectedAnswerAudioAction', () => ({
    playSelectedAnswerAudioAction: jest.fn().mockImplementation(() => Promise.resolve())
}));

describe('AnswersPage', () => {
    let answersPage, wrapper, store, testHistory;

    beforeEach(() => {
        store = createStore(
            rootReducer,
            applyMiddleware(thunk)
        );

        const options = {
            childContextTypes: {
                answerClusters: PropTypes.object.isRequired,
                history: PropTypes.object.isRequired,
            }
        };

        testHistory = createMemoryHistory();
        testHistory.push({
            pathname: '/sentiment'
        });
        testHistory.push({
            pathname: '/answers'
        });

        wrapper = mount(<Provider store={store}>
            <AnswersPage history={testHistory} />
        </Provider>, options);
        answersPage = wrapper.find('AnswersPage');
    });

    it("fetches state from store", () => {
        // Assert initial state
        expect(answersPage.props().answerClusters).toEqual([]);

        // Simulate reducers by populating the store with some data
        store.dispatch({
            type: GET_ANSWER_CLUSTERS,
            payload: ["blah"]
        });

        wrapper = wrapper.update();
        answersPage = wrapper.find('AnswersPage');

        // Assert that the state has been2 updated
        expect(answersPage.props().answerClusters).toEqual(["blah"]);
    });

    it("should render answer clusters", () => {
        store.dispatch({
            type: GET_ANSWER_CLUSTERS,
            payload: [["answer 1", "answer 2"], ["answer 3", "answer 4"]]
        });

        wrapper = wrapper.update();
        answersPage = wrapper.find('AnswersPage');

        expect(answersPage.find(AnswerClusterSelectionView).length).toBe(1);
        expect(answersPage.find(AnswerClusterSelectionView).find(MDBBtn).length).toBe(2);
    });

    it("selected answer on click should redirect to homepage", (done) => {
        // Preparation
        store.dispatch({
            type: GET_ANSWER_CLUSTERS,
            payload: [["answer 1", "answer 2"], ["answer 3", "answer 4"]]
        });

        wrapper = wrapper.update();
        answersPage = wrapper.find('AnswersPage');

        // Actual simulation
        answersPage
            .find(MDBBtn)
            .at(1)
            .simulate('click');

        wrapper = wrapper.update();
        answersPage = wrapper.find('AnswersPage');

        answersPage
            .find(MDBListGroupItem)
            .at(0)
            .simulate('click');

        // Assert that home page is now open
        setTimeout(() => {
            expect(playSelectedAnswerAudioAction).toHaveBeenCalled();
            expect(testHistory.entries.length).toBe(4);
            expect(testHistory.entries[0].pathname).toEqual("/");
            expect(testHistory.entries[1].pathname).toEqual("/sentiment");
            expect(testHistory.entries[2].pathname).toEqual("/answers");
            expect(testHistory.entries[3].pathname).toEqual("/");
            expect(testHistory.location.pathname).toEqual("/");
            done();
        }, 2000);
    });

    it("selecting answer should change isLoading state", () => {
        expect(answersPage.state().isLoading).toBe(false);

        // Preparation
        store.dispatch({
            type: GET_ANSWER_CLUSTERS,
            payload: [["answer 1", "answer 2"], ["answer 3", "answer 4"]]
        });

        wrapper = wrapper.update();
        answersPage = wrapper.find('AnswersPage');

        // Actual simulation
        answersPage
            .find(MDBBtn)
            .at(1)
            .simulate('click');

        wrapper = wrapper.update();
        answersPage = wrapper.find('AnswersPage');

        answersPage
            .find(MDBListGroupItem)
            .at(0)
            .simulate('click');

        // Assert that state has changed
        wrapper = wrapper.update();
        answersPage = wrapper.find('AnswersPage');

        expect(answersPage.state().isLoading).toBe(true);
    });
});