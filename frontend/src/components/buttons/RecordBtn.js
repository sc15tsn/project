import React, {Component} from 'react'
import {MDBBtn} from "mdbreact";
import PropTypes from "prop-types";

export default class RecordBtn extends Component {
    static propTypes = {
        isRecording: PropTypes.bool.isRequired,
        onClick: PropTypes.func.isRequired
    };

    render = () =>
        <MDBBtn color="dark" onClick={this.props.onClick}>
            {this.props.isRecording ? (
                <><i className="fas fa-stop fa-fw"/> Stop Recording</>) :
                (<><i className="fa fa-microphone fa-fw"/> Start Recording</>)}
        </MDBBtn>;
}