import React from 'react';
import {MDBBtn} from "mdbreact";
import SentimentSelectionBtn from './SentimentSelectionBtn';


describe('SentimentSelectionBtn', () => {
    let onClickSpy, sentimentSelectionBtnWrapper;

    beforeEach(() => {
        // Create a 'spy' (akin to a mock object)
        onClickSpy = jest.fn();

        // Create the record button object, passing the properties
        sentimentSelectionBtnWrapper = mount(<SentimentSelectionBtn sentiment="meh" onClick={onClickSpy} />);
    });

    it('renders an MDBBtn', () => {
        // Check that a single MDBBtn exists
        expect(sentimentSelectionBtnWrapper.find(MDBBtn).length).toEqual(1);
    });

    it('renders correct sentiment font"', () => {
        // Check that the button says 'Start Recording'
        expect(sentimentSelectionBtnWrapper.find('i').hasClass('far fa-meh')).toBe(true);
    });

    it('on click triggers on click action', () => {
        // Simulate a click
        sentimentSelectionBtnWrapper
            .find(MDBBtn)
            .simulate('click');

        // Check that start recording action was called
        expect(onClickSpy).toHaveBeenCalledTimes(1);
    });
});