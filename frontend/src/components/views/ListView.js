import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {MDBListGroup, MDBListGroupItem} from "mdbreact";

export default class ListView extends Component {
    static propTypes = {
        onClick: PropTypes.func.isRequired,
        items: PropTypes.array.isRequired
    };

    render() {
        return (
            <MDBListGroup>
                {this.props.items.map((item, i) =>
                    <MDBListGroupItem key={i}
                    onMouseDown={() => {
                        document.body.style.cursor = "default";
                    }} onClick={this.props.onClick}
                    onMouseEnter={() => {
                        document.body.style.cursor = "progress";
                    }} onMouseOver={()  => {
                        document.body.style.cursor = "pointer"
                    }} onMouseLeave={() => {
                        document.body.style.cursor = "default";
                    }}>{item}</MDBListGroupItem>
                )}
            </MDBListGroup>
        );
    }
}