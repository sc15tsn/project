import React from 'react';
import ListView from './ListView';
import AnswerClusterSelectionView from "./AnswerClusterSelectionView";
import {MDBBtn} from "mdbreact";

describe('AnswerClusterSelectionView', () => {
    it('calls onClick', () => {
        const clusters = [['a', 'b'], ['c', 'd']];

        // Create a 'spy' (akin to a mock object)
        const clickSpy = jest.fn();

        // Create the answer cluster selection view object, passing the spy as its onClick function
        const answerClusterSelectionView = mount(<AnswerClusterSelectionView clusters={clusters} onClick={clickSpy} />);

        // Simulate a click
        answerClusterSelectionView
            .find(MDBBtn)
            .at(1)
            .simulate('click');

        // Check that onClick was called
        expect(clickSpy).toHaveBeenCalled();
    });
});