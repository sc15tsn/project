import React from 'react';
import ListView from './ListView';

describe('ListView', () => {
    it('calls onClick', () => {
        const items = ['a', 'b', 'c'];

        // Create a 'spy' (akin to a mock object)
        const clickSpy = jest.fn();

        // Create the list view object, passing the spy as its onClick function
        const listView = mount(<ListView items={items} onClick={clickSpy} />);

        // Simulate a click
        listView
            .find('li')
            .first()
            .simulate('click');

        // Check that onClick was called
        expect(clickSpy).toHaveBeenCalled();
    });

    it('populates list items', () => {
        const items = ['a', 'b', 'c'];

        // Create the list view object, passing the spy as its onClick function
        const listView = mount(<ListView items={items} onClick={sinon.spy()} />);

        const nodes = listView.find('li');

        // Check that the list contains 3 items
        expect(nodes.length).toEqual(3);

        // Check that the list contains the correct items
        expect(nodes.at(0).text()).toEqual('a');
        expect(nodes.at(1).text()).toEqual('b');
        expect(nodes.at(2).text()).toEqual('c');

        // Double-check that the list doesn't contain more than 3 items
        expect(nodes.at(3).length).toEqual(0);
    });
});