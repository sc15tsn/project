import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {MDBBadge, MDBBtn, MDBCol, MDBRow} from "mdbreact";

export default class AnswerClusterSelectionView extends Component {
    static propTypes = {
        onClick: PropTypes.func.isRequired,
        clusters: PropTypes.array.isRequired
    };

    render = () =>
        <MDBRow className="row" center>
            {this.props.clusters.map((item, i) =>
                <MDBCol key={i} xs="12" md="6">
                    <MDBBtn id={i} onClick={this.props.onClick}>
                        {item[0]}<MDBBadge color="primary" pill>+ {item.length - 1} more</MDBBadge>
                    </MDBBtn>
                </MDBCol>
            )}
        </MDBRow>;
}