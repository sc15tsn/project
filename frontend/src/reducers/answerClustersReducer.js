import {RESET_ALL, GET_ANSWER_CLUSTERS} from "../actions/types";

const initialState = [];

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_ANSWER_CLUSTERS:
            return action.payload;

        case RESET_ALL:
            return initialState;

        default:
            return state;
    }
}