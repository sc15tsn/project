#!/bin/bash
. /app/models/model.cfg.sh # Load config
. /app/models/download_pretrained.sh # Download models if missing

DATA_DIR="/app/dataset"
DATA_FORMAT="tsv"

shopt -s nocasematch
echo "Training with BERT fine-tuning..."
. train/train_docker.sh ${DATA_DIR} ${DATA_FORMAT}
echo "Training complete"
