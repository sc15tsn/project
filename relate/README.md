## Training

Refer to `train` service
## Predicting
Prediction is the default mode when container is being ran,
just simply use `docker-compose up relate` and prediction will be available
on `127.0.0.1:8001` or `relate:8000` if called from another container. 

This endpoint takes in `query` and `answers` as its parameters,
which you can specify either in JSON body, 
or alternatively as an HTTP query 
(separated as commas, thus no commas within sentence are allowed if HTTP query is used).

## Notes 
Because Tensorflow image for docker uses `3.5` version of Python, 
we can't use `f'` strings introduced in `3.6`.
 As an alternative use `format()` (more info [here](https://www.python.org/dev/peps/pep-0498/))