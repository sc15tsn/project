"""HTTP API interface for Relater"""
from typing import Any

import falcon

from relate import relater, log
from relate.constants import HTTP_QUERY_PARAMS_ENABLED, DEFAULT_THRESHOLD, DEFAULT_MAX_ANSWERS, \
    DEFAULT_CLUSTER_EPS

LOGGER = log.get_logger(__name__)


class RelateResource:
    """Resource object for relate API"""

    def __init__(self) -> None:
        self.relater = relater.Relater()

    @staticmethod
    def get_param(param_name: str,
                  req: falcon.Request,
                  required: bool = True,
                  allow_query_param: bool = HTTP_QUERY_PARAMS_ENABLED,
                  as_list: bool = False) -> Any:
        """Get the parameter from either JSON body or HTTP Query params (if enabled)

        Args:
            param_name: Parameter name.
            req: falcon.Request object where the param_name will be retrieved from.
            resp: falcon.Response object.
            required: True if it is required to retrieve the parameter.
                If it is required and parameter is not specified, it will
                change the resp with HTTP_BAD_REQUEST and an appropriate message.
            allow_query_param: True if HTTP Query parameters should be allowed.
            as_list: True if parameter should be retrieved as list.

        Returns:
            Parameter if found in the request, otherwise None.
        """
        try:
            # Get the parameter from JSON body
            param = req.media.get(param_name)
        except (AttributeError, falcon.errors.HTTPBadRequest, falcon.errors.HTTPUnsupportedMediaType):

            if not as_list:
                # Get the parameter from HTTP Query if not found in JSON body
                param = req.get_param(param_name) if allow_query_param else None
            else:
                param = req.get_param_as_list(param_name) if allow_query_param else None

        if param is None and required:
            raise falcon.HTTPBadRequest(description="You have to specify '%s' parameter." % param_name)

        return param

    def on_any(self, req: falcon.Request, resp: falcon.Response) -> None:
        """Calls the relate method.

        Args:
            req: falcon.Request with `query` and `answers` parameters specified.
            resp: falcon.Response

        Notes:
            if successful the response body contains `answer_clusters`.
        """

        query = self.get_param('query', req)
        answers = self.get_param('answers', req, as_list=True)
        threshold = self.get_param('threshold', req, required=False)
        max_answers = self.get_param('max_answers', req, required=False)
        cluster_eps = self.get_param('cluster_eps', req, required=False)

        threshold = threshold if threshold else DEFAULT_THRESHOLD
        max_answers = max_answers if max_answers else DEFAULT_MAX_ANSWERS
        cluster_eps = cluster_eps if cluster_eps else DEFAULT_CLUSTER_EPS

        related_answers = self.relater.relate(query, answers,
                                              threshold=threshold,
                                              max_answers=max_answers,
                                              cluster_eps=cluster_eps)

        resp.media = {'answer_clusters': related_answers}

    def on_get(self, req: falcon.Request, resp: falcon.Response) -> None:
        """Redirects to on_all"""
        self.on_any(req, resp)

    def on_post(self, req: falcon.Request, resp: falcon.Response) -> None:
        """Redirects to on_all"""
        self.on_any(req, resp)


def create() -> falcon.API:
    """Initialises the API.

    Returns:
        falcon.API
    """
    api = falcon.API()
    res = RelateResource()
    api.add_route('/', res)

    LOGGER.info("Relate API is ready.")

    return api


relate_API = create()
