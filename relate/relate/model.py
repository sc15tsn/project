"""A set of functions for regression model initialization and input preprocessing"""
from typing import List, Union, Mapping

import tensorflow as tf
from bert import modeling, tokenization
from bert.modeling import BertConfig
from bert.run_classifier import InputExample

from relate.constants import RELEVANT_LABEL


def model_fn_builder(bert_config: BertConfig):
    """Builds model_fn

    Args:
        bert_config: BERT configuration object

    Returns:
        model_fn
    """

    def model_fn(features: Mapping[str, List[List[int]]],
                 labels: None = None,  # pylint: disable=unused-argument
                 params: None = None) -> tf.estimator.EstimatorSpec:  # pylint: disable=unused-argument
        """The `model_fn` for tf.Estimator.

        Args:
            features: List of feature dicts (input_ids, input_mask, segment_ids).
            labels: Placeholder that is required by tf.estimator.
            params: Placeholder for batch_size to be initialised later.

        Returns:
            tf.Estimator.EstimatorSpec object for predictions
        """
        input_ids = features["input_ids"]
        input_mask = features["input_mask"]
        segment_ids = features["segment_ids"]

        probs, answer_embedding = create_model(bert_config,
                                                input_ids,
                                                input_mask,
                                                segment_ids)

        predictions = {
            'probability': probs,
            'answer_embedding': answer_embedding,
        }
        return tf.estimator.EstimatorSpec(tf.estimator.ModeKeys.PREDICT, predictions=predictions)

    return model_fn


def create_model(bert_config: BertConfig,
                 input_ids: Union[tf.Tensor, List[List[int]]],
                 input_mask: Union[tf.Tensor, List[List[int]]],
                 segment_ids: Union[tf.Tensor, List[List[int]]]) -> (tf.Tensor, tf.Tensor):
    """Creates a regression model.

    bert_config: `BertConfig` instance.
    is_training (bool): True for training model, False for eval model. Controls
        whether dropout will be applied.
    input_ids: int32 Tensor of shape [batch_size, seq_length].
    input_mask: (optional) int32 Tensor of shape [batch_size, seq_length].
    segment_ids: (optional) int32 Tensor of shape [batch_size, seq_length].
    """
    model = modeling.BertModel(
        config=bert_config,
        is_training=False,
        input_ids=input_ids,
        input_mask=input_mask,
        token_type_ids=segment_ids,
        use_one_hot_embeddings=False)

    bert_output_layer = model.get_pooled_output()

    hidden_size = bert_output_layer.shape[-1].value

    output_weights = tf.get_variable(
        "output_weights", [2, hidden_size],
        initializer=tf.truncated_normal_initializer(stddev=0.02))

    output_bias = tf.get_variable(
        "output_bias", [2], initializer=tf.zeros_initializer())

    with tf.variable_scope("loss"):
        logits = tf.matmul(bert_output_layer, output_weights, transpose_b=True)
        logits = tf.nn.bias_add(logits, output_bias)

        probs = tf.nn.softmax(logits, axis=-1)[:, 0]

        # Find the index of second [SEP] token (input_id=102)
        second_sep_idx = tf.where(tf.equal(input_ids, 102))[-1][0]

        # The answer embeddings are represented as the second [SEP] token in the output sequence.
        second_sep_embedding = model.get_sequence_output()[:, second_sep_idx, :]

        return probs, second_sep_embedding


def create_examples(query: str, answers: List[str]) -> List[InputExample]:
    """Creates example representation for predictions.

    Args:
        query: Conversation partner's query.
        answers: Potential answers.

    Returns:
        A list of InputExamples objects that can be transformed to features for BERT.
    """
    examples = []
    query = tokenization.convert_to_unicode(query)
    for (i, answer) in enumerate(answers):
        answer = tokenization.convert_to_unicode(answer)
        examples.append(InputExample(guid=i,  # Doesn't matter as long as it is unique
                                     text_a=query,
                                     text_b=answer,
                                     label=RELEVANT_LABEL  # This will be ignored anyway
                                     ))
    return examples
