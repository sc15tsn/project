"""Contains the dataset generator for both samples and answers."""
import itertools
import multiprocessing
import subprocess
from typing import *
import math
import os

import rapidjson
import re
import html


class DatasetGenerator:
    """Generates an AICO dataset from the reddit comment dataset.

    Attributes:
        input_filepath: Unprocessed reddit dataset filename.
        num_lines: Number of total file lines to process.
        lines_per_chunk: Number of lines per chunk (for processing large files).
        num_chunks: Total number of chunks to process.
        current_chunk Number of chunks processed so far.
        starting_chunk: The starting chunk. Used for resuming cancelled runs.
        substring_blacklist: A list of substrings that if contained within a comment, will discard the comment.
        comment_id_to_body: Dictionary mapping comment ids to bodies.
        comment_id_to_child_ids: Dictionary mapping comment ids to list of child ids.
        comment_count: Number of comments to process.
        verbose: True if verbose output should be printed.
    """

    def __init__(self, input_filepath: str, num_lines: int = None, lines_per_chunk: int = None,
                 starting_chunk: int = 0,
                 verbose: bool = False):

        self.comment_count = 0
        self.comment_id_to_body = {}
        self.comment_id_to_child_ids = {}
        self.reddit_thread_to_comments = {}
        self.starting_chunk = starting_chunk
        self.cleanup_regexp = re.compile(r'[\n\t]+')
        self.manager = multiprocessing.Manager()

        # Get the substring blaclist from file.
        with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'blacklist.txt'), 'r') as f:
            self.substring_blacklist = [l.strip() for l in f.readlines()]

        # Check that file exists
        file = open(input_filepath, "r")
        file.close()

        self.input_filepath = input_filepath
        self.verbose = verbose

        if num_lines is not None:
            if num_lines <= 0:
                raise ValueError("'num_lines' parameter must be positive.")
            self.num_lines = num_lines
        else:
            self.verbose_print(
                f"Counting number of lines in \"{self.input_filepath}\" (this may take a while).")
            self.num_lines = self.count_input_file_lines()

        if lines_per_chunk is not None:
            if lines_per_chunk <= 0:
                raise ValueError("'lines_per_chunk' parameter must be positive.")
            if lines_per_chunk > self.num_lines:
                raise ValueError("'lines_per_chunk' parameter must be <= 'num_lines' parameter.")
            self.lines_per_chunk = lines_per_chunk
        else:
            self.lines_per_chunk = self.num_lines

        self.num_chunks = math.ceil(self.num_lines / self.lines_per_chunk)

        self.verbose_print("|DATASET SETTINGS|")
        self.verbose_print(f"Unprocessed dataset: \"{self.input_filepath}\"")
        self.verbose_print(f"Lines to use: {self.num_lines}")
        self.verbose_print(f"Lines per chunk: {self.lines_per_chunk}\n")

    def verbose_print(self, *values, **kwargs):
        if self.verbose:
            print(*values, **kwargs)

    def extract_chunk_file_data(self, max_chars: Union[int, None], children: bool):
        """Extracts relevant JSON data from a file into a buffer.

        Args:
            max_chars: Character limit on comments that are included
            children: Whether or not to extract child id data in addition to comment comment_id_to_body.
        """
        with open(self.input_filepath, 'r') as input_file:
            start = self.starting_chunk * self.lines_per_chunk
            end = min(start + self.lines_per_chunk, self.num_lines)
            for line in itertools.islice(input_file, start, end):
                try:
                    self.extract_line_data(line, max_chars, children)
                except ValueError:
                    continue
        if children:
            self.prune_child_ids()

    def extract_line_data(self, line: str, max_chars: Union[int, None], children: bool):
        """Extracts relevant JSON data from a line of a file into a buffer.

        Args:
            line: Current line in file we are processing.
            max_chars: Character limit on comments that are included.
            children: Whether or not to extract child id data in addition to comment body.
        """
        json_line = rapidjson.loads(line)
        if not self.is_comment_valid(json_line, max_chars):
            return

        self.comment_id_to_body[json_line["id"]] = \
            html.unescape(re.sub(self.cleanup_regexp, ' ', json_line["body"]))
        if children and json_line["parent_id"][:3] == "t1_":
            if json_line["parent_id"][3:] in self.comment_id_to_child_ids:
                self.comment_id_to_child_ids[json_line["parent_id"][3:]].append(json_line["id"])
            else:
                self.comment_id_to_child_ids[json_line["parent_id"][3:]] = [json_line["id"]]

    def is_comment_valid(self, comment: Dict[str, str], max_chars: Union[int, None]) -> bool:
        """Determines whether a comment is suitable for use.

        Args:
            comment: The comment to validate.
            max_chars: Character limit on comments that are valid.
        Returns:
            True if the comment body is below the char limit (or no limit is set),
                and if the body is not deleted.
            False otherwise.
        """
        # Valid length
        if max_chars is not None and len(comment["body"]) > max_chars:
            return False

        # Does not contain any forbidden substrings
        if any(substring in comment["body"] for substring in self.substring_blacklist):
            return False

        return True

    def prune_child_ids(self):
        """Removes items in the child-ids buffer that do not have comment_id_to_body i.e. cannot be used."""
        del_list = []
        for parent in self.comment_id_to_child_ids.keys():
            if parent not in self.comment_id_to_body:
                del_list.append(parent)

        for parent in del_list:
            del self.comment_id_to_child_ids[parent]

    def count_input_file_lines(self):
        """Counts the number of lines in the input file."""
        return int(subprocess.check_output('wc -l {}'.format(self.input_filepath), shell=True).split()[0])

    def sort_input_file_by_reddit_threads(self, filename: str, replace: bool = False):
        """Sorts the input file by reddit thread.

        Args:
            filename: name to use for the sorted file
            replace: use the sorted file for further dataset generation operations
        """
        comment_id_to_input_json = {}

        with open(self.input_filepath, "r") as unsorted_reddit_comments:
            for line in unsorted_reddit_comments:
                try:
                    json_line = rapidjson.loads(line)
                except ValueError:
                    continue
                id = json_line["id"]
                link_id = json_line["link_id"]
                comment_id_to_input_json[id] = json_line
                if link_id in self.reddit_thread_to_comments:
                    self.reddit_thread_to_comments[link_id].append(id)
                else:
                    self.reddit_thread_to_comments[link_id] = [id]

        with open(filename, "w") as sorted_reddit_comments:
            for comment_ids in self.reddit_thread_to_comments.values():
                comments = []
                for comment_id in comment_ids:
                    comments.append(f'{rapidjson.dumps(comment_id_to_input_json[comment_id])}\n')
                sorted_reddit_comments.writelines(comments)

        if replace:
            self.input_filepath = filename

    def write_to_json_file(self, data: List[Dict[str, str]], filepath: str, mode: str):
        """Writes data out to a specified file in json format.

        Args:
            data: Data to write.
            filepath: String containing the name of the file to write to.
            mode: String containing file mode.
        """
        with open(filepath, mode) as output_file:
            for sample in data:
                rapidjson.dump(sample, output_file)
                output_file.write("\n")
