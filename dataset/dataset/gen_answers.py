"""Contains the dataset generator for the sentiment tagged answers, and a script to launch it."""
import argparse
import math
import multiprocessing
import os

from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

from dataset.dataset_generator import DatasetGenerator


class AnswerGenerator(DatasetGenerator):
    """Generates sentiment tagged answers from the reddit comment dataset.

    Attributes:
         answers: List of comments and their sentiments. Each answer is a dict of the form:
            {"text": "...", "sentiment": "pos"/"pos-neu"/"neu"/"neg-neu"/"neg"}
    """
    def __init__(self, input_filepath: str, num_lines: int = None, lines_per_chunk: int = None,
                 starting_chunk: int = 0,
                 verbose: bool = False):
        super(AnswerGenerator, self).__init__(input_filepath, num_lines, lines_per_chunk, starting_chunk,
                                              verbose)
        self.answers_lock = self.manager.Lock()
        self.answers = self.manager.list()

    def reset_answers(self):
        """Resets all state relating to answer generation."""
        self.comment_count = 0
        self.answers = self.manager.list()
        self.comment_id_to_body = {}
        self.comment_id_to_child_ids = {}

    def generate_answers(self, output_dir: str, max_chars: int = None, fmt: str = "json",
                         n_processes: int = 1):
        """Generates a list of answers and their sentiments.

        Args:
            output_dir: Name of the file to write answers to
            max_chars: Integer character limit for comments that are included as answers
            fmt: Format of the output file
            n_processes: Number of processes to use
        """
        if n_processes < 1:
            raise ValueError("'n_processes' must be at least 1")

        output_filepath = f'{output_dir}/answers.{fmt}'
        for self.current_chunk in range(self.starting_chunk, self.num_chunks):
            self.reset_answers()
            self.verbose_print(f"[Chunk {self.starting_chunk} out of {self.num_chunks - 1}] "
                               f"({(self.starting_chunk / self.num_chunks) * 100:.2f}%)")
            self.extract_chunk_file_data(max_chars, children=False)
            self.comment_count = len(self.comment_id_to_body)

            processes = []
            process_start = 0
            default_process_size = math.ceil(self.comment_count / n_processes)
            for _ in range(n_processes):
                if default_process_size <= len(list(self.comment_id_to_body)[process_start:]):
                    process_size = default_process_size
                else:
                    process_size = len(list(self.comment_id_to_body)[process_start:])
                process = multiprocessing.Process(target=self.get_partial_answers,
                                                  args=(process_start, process_size))
                process.start()
                processes.append(process)
                process_start += process_size

            for process in processes:
                process.join()

            self.verbose_print(f'Writing {len(self.answers)} answers to "{output_filepath}".\n')

            mode = "w" if self.starting_chunk == 0 else "a"
            self.write_to_json_file(self.answers, output_filepath, mode)

    def get_partial_answers(self, start: int, size: int):
        """Generates sentiment tagged answers for a portion of the currently loaded chunk.

        Args:
            start: where to begin processing
            size: how many comments to process
        """
        local_answers = []

        for i in range(start, start + size):
            answer = dict()
            answer["text"] = list(self.comment_id_to_body.values())[i]
            answer["sentiment"] = self.get_sentiment(answer["text"])

            local_answers.append(answer)

        with self.answers_lock:
            self.answers.extend(local_answers)

    def get_sentiment(self, text: str) -> float:
        """Calculates the sentiment of a given text.

        Args:
            text: The text to perform sentiment analysis on.
        Returns:
            The sentiment value -1.0 (very negative) to 1.0 (very positive)
        """
        analyser = SentimentIntensityAnalyzer()
        return analyser.polarity_scores(text)["compound"]


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Generate formatted reddit dataset answers with sentiment.")
    parser.add_argument("input_file", type=str, help="path to the JSON input file.")
    parser.add_argument("-num_lines", type=int, default=None,
                        help="number of input lines to process, (default: all lines).")
    parser.add_argument("-lines_per_chunk", type=int, default=None,
                        help="number of lines to process at a time (default: all lines).")
    parser.add_argument("-max_chars", type=int, default=None,
                        help="character limit on comments to use in samples (default: no limit).")
    parser.add_argument("-output_format", type=str, default="tsv",
                        help="format to output samples in (json, csv, tsv) (default: tsv).")
    parser.add_argument("-n_processes", type=int, default=os.cpu_count(),
                        help="number of processes to use (default: number of cores)")
    parser.add_argument("-starting_chunk", type=int, default=0,
                        help="The chunk to start from. Used for resuming cancelled jobs (default: 0).")
    parser.add_argument("-output_dir", type=str, default="out", help="output dir")
    parser.add_argument("-v", default=False, action="store_true", help="verbose")

    args = parser.parse_args()

    dg = AnswerGenerator(input_filepath=args.input_file, num_lines=args.num_lines,
                         lines_per_chunk=args.lines_per_chunk, starting_chunk=args.starting_chunk,
                         verbose=args.v)
    dg.generate_answers(output_dir=args.output_dir, max_chars=args.max_chars,
                        fmt=args.output_format, n_processes=args.n_processes)
