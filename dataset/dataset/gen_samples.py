"""Contains the dataset generator for the training samples, and a script to launch it."""
import argparse
import csv
import math
import os
import random
from typing import Union, List, Dict

from dataset.dataset_generator import DatasetGenerator


class SampleGenerator(DatasetGenerator):
    """Generates training sample dataset from the reddit comment dataset.

    Attributes:
        positive_samples: List of positive samples. Each sample is a dict of the form:
            {"query": "...", "answer": "...", "relevant": "yes"}
        negative_samples: List of negative samples. Each sample is a dict of the form:
            {"query": "...", "answer": "...", "relevant": "no"}

        train_samples: List of training samples.
        dev_samples: List of validation samples.
        eval_samples: List of eval samples.
        num_positives: Dictionary mapping comment ids to number of positives generated.
        num_negatives: Dictionary mapping comment ids to number of negatives generated.
    """

    def __init__(self, input_filepath: str, num_lines: int = None, lines_per_chunk: int = None,
                 starting_chunk: int = 0,
                 verbose: bool = False):
        super(SampleGenerator, self).__init__(input_filepath, num_lines, lines_per_chunk, starting_chunk,
                                              verbose)
        self.positive_samples = []
        self.negative_samples = []
        self.train_samples = []
        self.dev_samples = []
        self.eval_samples = []
        self.num_positives = {}
        self.num_negatives = {}
        self.rows_processed = 0

    def print_samples_info(self, verbose_only: bool = False):
        """Prints out the distribution of positive and negative samples.

        Args:
            verbose_only: True if it should only printed when verbose is on.
        """
        if not self.verbose and verbose_only:
            return

        positives = sum(self.num_positives.values())
        negatives = sum(self.num_negatives.values())
        pos_percent = ((float(positives) / (
                positives + negatives)) * 100) if positives + negatives > 0 else 0
        neg_percent = 100 - pos_percent if pos_percent > 0 else 0
        print(
            f"Generated {positives + negatives} samples. {positives} positives ({'%.2f' % pos_percent}%), "
            f"{negatives} negatives ({'%.2f' % neg_percent}%).")

    def generate_samples(self,
                         output_dir: str = 'out',
                         train_sample_ratio: Union[float, None] = 0.8,
                         dev_sample_ratio: Union[float, None] = None,
                         eval_sample_ratio: Union[float, None] = None,
                         neg_ratio: float = 1,
                         max_chars: Union[int, None] = None,
                         seed: Union[int, None] = None,
                         output_format: str = "json"):
        """Generates relevancy dataset from reddit conversation dataset.

        Args:
            output_dir: Output directory.
            train_sample_ratio: Ratio of samples to use for training.
            dev_sample_ratio: Ratio of samples to use for validation.
            eval_sample_ratio: Ratio of samples to use for evaluation.
            neg_ratio: Number of negatives that should be generated for each positive.
            max_chars: Max number of characters a comment should have to be considered.
            seed: Seed used for pseudo random operations.
            output_format: Output format for samples.
        Raises:
            ValueError: If parameters are outside of expected range.
        """

        if train_sample_ratio is None:
            raise ValueError("Must have at least train ratio specified.")

        # Split dev/val into what is left from train
        if dev_sample_ratio is None and eval_sample_ratio is None:
            dev_sample_ratio = eval_sample_ratio = (1 - train_sample_ratio) / 2

        if dev_sample_ratio is None:
            dev_sample_ratio = 1 - train_sample_ratio - eval_sample_ratio
        if eval_sample_ratio is None:
            eval_sample_ratio = 1 - train_sample_ratio - dev_sample_ratio

        if not math.isclose(train_sample_ratio + dev_sample_ratio + eval_sample_ratio, 1):
            raise ValueError("train/dev/dev ratios must sum up to 1"
                             f"got {train_sample_ratio + dev_sample_ratio + eval_sample_ratio} instead."
                             f"({train_sample_ratio}/{dev_sample_ratio}/{eval_sample_ratio})")

        if train_sample_ratio < 0 or train_sample_ratio > 1:
            raise ValueError("'train_sample_ratio' parameter must be within the range [0, 1].")

        if dev_sample_ratio < 0 or dev_sample_ratio > 1:
            raise ValueError("'dev_sample_ratio' parameter must be within the range [0, 1].")

        if eval_sample_ratio < 0 or eval_sample_ratio > 1:
            raise ValueError("'eval_sample_ratio' parameter must be within the range [0, 1].")

        if neg_ratio < 0:
            raise ValueError("'neg_ratio' parameter must be non-negative.")

        if max_chars is not None and max_chars < 0:
            raise ValueError("'max_chars' parameter must be non-negative.")

        self.verbose_print("|GENERATION SETTINGS|")
        self.verbose_print(
            f"train/dev/eval split: {train_sample_ratio}/{dev_sample_ratio}/{eval_sample_ratio}")
        self.verbose_print(f"Negatives per positive: {neg_ratio}")
        self.verbose_print(f"Comment character limit: {max_chars}")
        self.verbose_print(f"Given random seed: {seed}\n")

        random.seed(seed)

        for self.current_chunk in range(self.starting_chunk, self.num_chunks):
            self.reset_samples()
            self.verbose_print(f"[Chunk {self.current_chunk} out of {self.num_chunks - 1}] "
                               f"({(self.current_chunk / self.num_chunks) * 100:.2f}%)")
            self.extract_chunk_file_data(max_chars, children=True)
            self.comment_count = len(self.comment_id_to_child_ids)
            self.generate_all_samples(neg_ratio)
            self.print_samples_info(verbose_only=True)
            self.shuffle_and_split_samples(train_sample_ratio, dev_sample_ratio, eval_sample_ratio)
            self.export_all_samples(output_dir, output_format)

    def reset_samples(self):
        """Resets all state relating to sample generation."""
        self.comment_count = 0
        self.rows_processed = 0
        self.comment_id_to_body = {}
        self.comment_id_to_child_ids = {}
        self.positive_samples = []
        self.negative_samples = []
        self.train_samples = []
        self.dev_samples = []
        self.eval_samples = []
        self.num_positives = {}
        self.num_negatives = {}

    def shuffle_and_split_samples(self,
                                  train_ratio: float,
                                  dev_ratio: float,
                                  eval_ratio: float):
        """Shuffles and splits the samples into train, dev and eval sets.

        Args:
            train_ratio: Ratio of samples to put in training set.
            dev_ratio: Ratio of samples to put in validation set.
            eval_ratio: Ratio of samples to put in eval set.
        """

        assert train_ratio + dev_ratio + eval_ratio == 1

        samples = self.positive_samples + self.negative_samples

        random.shuffle(samples)

        num_train_samples = math.floor(len(samples) * train_ratio)
        num_dev_samples = math.floor(len(samples) * dev_ratio)
        # The rest are eval samples

        self.train_samples = samples[0:num_train_samples]
        self.dev_samples = samples[
                           num_train_samples:num_train_samples + num_dev_samples]
        self.eval_samples = samples[num_train_samples + num_dev_samples:]

    def export_all_samples(self, output_dir: str, fmt: str):
        """Writes the training and eval sets out to given files in a given format.

        Args:
            output_dir: Directory to write output files to.
            fmt: Format of output files (JSON, CSV, TSV).
        """
        train_filepath = f'{output_dir}/train.{fmt}'
        dev_filepath = f'{output_dir}/dev.{fmt}'
        eval_filepath = f'{output_dir}/eval.{fmt}'

        os.makedirs(output_dir, exist_ok=True)

        self.verbose_print(
            f'Writing {len(self.train_samples)} training samples to "{train_filepath}".')
        self.write_samples_to_file(samples=self.train_samples, filepath=train_filepath,
                                   fmt=fmt)
        self.verbose_print(
            f'Writing {len(self.dev_samples)} validation samples to "{dev_filepath}".')
        self.write_samples_to_file(samples=self.dev_samples, filepath=dev_filepath,
                                   fmt=fmt)
        self.verbose_print(f'Writing {len(self.eval_samples)} eval samples to "{eval_filepath}".\n')
        self.write_samples_to_file(samples=self.eval_samples, filepath=eval_filepath, fmt=fmt)

    def write_samples_to_file(self, samples: List[Dict[str, str]], filepath: str, fmt: str):
        """Writes list of samples out to a specified file in a given format.

        Args:
            samples: List of dictionaries to write.
            filepath: Name of the file to write to.
            fmt: Output format.
        Raises:
            ValueError: If the format string does not match one of the supported types.
        """
        mode = "w" if self.starting_chunk == 0 else "a"

        # Write samples to file
        if fmt == "json":
            self.write_to_json_file(samples, filepath=filepath, mode=mode)
        elif fmt == "csv":
            self.write_to_dsv_file(samples, filepath=filepath, mode=mode,
                                   delimiter=',')
        elif fmt == "tsv":
            self.write_to_dsv_file(samples, filepath=filepath, mode=mode,
                                   delimiter="\t")
        else:
            raise ValueError("Given format is not supported.")

    def write_to_dsv_file(self, samples: List[Dict[str, str]], filepath: str, mode: str,
                          delimiter: str):
        """Writes list of samples out to a specified file in delimiter separated value format.

        Args:
            samples: List of dictionaries to write.
            filepath: String containing the name of the file to write to.
            mode: String containing file mode.
            delimiter: Character to use as delimiter.
        """
        with open(filepath, mode) as output_file:
            writer = csv.writer(output_file, delimiter=delimiter)
            lines = [(sample['query'].replace('\n', ' '), sample['answer'].replace('\n', ' '),
                      sample['relevant']) for sample in samples]
            writer.writerows(lines)

    def print_progress(self, verbose_only: bool = False, every_n_steps: int = 100):
        """Prints out the progress of the dataset generation process.

        Args:
            verbose_only: True if it should only printed when verbose is on.
            every_n_steps: n for printing every n steps
        """
        if (not self.verbose and verbose_only) or self.rows_processed % every_n_steps != 0:
            return

        progress_percent = float(
            self.rows_processed) / self.comment_count * 100 if self.comment_count > 0 else 0
        print(
            f"\r{self.rows_processed} / {self.comment_count} rows processed ({progress_percent:.2f}).")
        if self.rows_processed == self.comment_count:
            print("\n")

    def generate_all_samples(self, neg_ratio: float):
        """Generates positive and negative samples for each comment.

        Args:
            neg_ratio: Number of negative samples to generate per positive.
        """
        for parent in self.comment_id_to_child_ids.keys():
            self.generative_positive_samples(parent)
            self.generate_negative_samples(parent, neg_ratio)
            self.rows_processed += 1
            self.print_progress(True)

    def generative_positive_samples(self, parent: str):
        """Generates positive samples for a single comment.

        Args:
            parent: The id of the comment we are generating positives for.
        """
        for child in self.comment_id_to_child_ids[parent]:
            self.positive_samples.append(
                {"query": self.comment_id_to_body[parent], "answer": self.comment_id_to_body[child],
                 "relevant": "yes"})
        self.num_positives[parent] = len(self.comment_id_to_child_ids[parent])

    def generate_negative_samples(self, parent: str, neg_ratio: float):
        """Generates negatives for a single comment.

        Args:
            parent: The id of the comment we are generating negatives for.
            neg_ratio: Number of negative samples to generate per positive.
        """
        neg_sample_size = math.floor(self.num_positives[parent] * neg_ratio)
        neg_sample = []
        neg_count = 0

        # If the sample size is bigger than the number of non-children, use as many non-children as possible
        if len(self.comment_id_to_body) - len(self.comment_id_to_child_ids[parent]) <= neg_sample_size:
            neg_sample = [x for x in self.comment_id_to_body.keys() if
                          x not in self.comment_id_to_child_ids[parent]]

        # Otherwise, it is faster to randomly sample keys, and check that they are non-children afterwards
        else:
            candidates = list(self.comment_id_to_body.keys())
            while True:
                if neg_count == neg_sample_size:
                    break
                potential_neg = random.choice(candidates)
                if potential_neg not in self.comment_id_to_child_ids[parent] \
                        and potential_neg not in neg_sample:
                    neg_sample.append(potential_neg)
                    neg_count += 1
                else:
                    candidates.remove(potential_neg)

        for non_child in neg_sample:
            self.negative_samples.append(
                {"query": self.comment_id_to_body[parent], "answer": self.comment_id_to_body[non_child],
                 "relevant": "no"})
        self.num_negatives[parent] = len(neg_sample)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate formatted reddit dataset samples.")
    parser.add_argument("input_file", type=str, help="path to the JSON input file.")
    parser.add_argument("-num_lines", type=int, default=None,
                        help="number of input lines to process.")
    parser.add_argument("-lines_per_chunk", type=int, default=None,
                        help="number of lines to process at a time (default: all lines).")
    parser.add_argument("-train_ratio", type=float, default=0.8,
                        help="ratio of train samples (default: 0.8).")
    parser.add_argument("-dev_ratio", type=float, default=0.1,
                        help="ratio of validation samples (default: 0.1).")
    parser.add_argument("-eval_ratio", type=float, default=0.1,
                        help="ratio of evaluation samples (default: 0.1).")
    parser.add_argument("-neg_ratio", type=float, default=1,
                        help="number of negative samples to generate per positive (default: 1).")
    parser.add_argument("-max_chars", type=int, default=None,
                        help="character limit on comments to use in samples (default: no limit).")
    parser.add_argument("-seed", default=None,
                        help="seed for random operations (default: random).")
    parser.add_argument("-output_format", type=str, default="json",
                        help="format to output samples in (json, csv, tsv) (default: json).")
    parser.add_argument("-starting_chunk", type=int, default=0,
                        help="The chunk to start from. Used for resuming cancelled jobs (default: 0).")
    parser.add_argument("-output_dir", type=str, default="out", help="Output directory (default: out).")
    parser.add_argument("-v", default=False, action="store_true", help="verbose.")

    args = parser.parse_args()

    dg = SampleGenerator(input_filepath=args.input_file, num_lines=args.num_lines,
                         lines_per_chunk=args.lines_per_chunk, starting_chunk=args.starting_chunk,
                         verbose=args.v)
    dg.generate_samples(output_dir=args.output_dir,
                        train_sample_ratio=args.train_ratio,
                        dev_sample_ratio=args.dev_ratio,
                        eval_sample_ratio=args.eval_ratio,
                        neg_ratio=args.neg_ratio,
                        max_chars=args.max_chars,
                        seed=args.seed,
                        output_format=args.output_format)
