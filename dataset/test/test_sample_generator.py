import math
import os

import pytest

from dataset.gen_samples import SampleGenerator

TEST_PATH = os.path.dirname(os.path.abspath(__file__))
MOCK_DATASET = TEST_PATH + "/mock_reddit_dataset.json"
THREAD_MOCK_DATASET = TEST_PATH + "/mock_reddit_thread_dataset.json"

CORRECT_CHILD_IDS = \
    {0: [], 1: [], 2: [], 3: [], 4: [11],
     5: [23, 28], 6: [], 7: [3, 8, 21], 8: [6], 9: [24],
     10: [], 11: [], 12: [18, 25], 13: [26], 14: [],
     15: [], 16: [], 17: [0, 1], 18: [16, 20], 19: [],
     20: [2, 22], 21: [], 22: [15], 23: [], 24: [9],
     25: [12], 26: [13, 14, 19], 27: [7, 29], 28: [27], 29: [17]}

COMMENT_BODY = \
    {0: "test1",
     1: "test2 long",
     2: "test3",
     3: "test4 long",
     4: "test5",
     5: "test6 long",
     6: "test7",
     7: "test8 long",
     8: "test9",
     9: "test10 long",
     10: "test11",
     11: "test12 long",
     12: "test13",
     13: "test14 long",
     14: "test15",
     15: "test16 long",
     16: "test17",
     17: "test18 long",
     18: "test19",
     19: "test20 long",
     20: "test21",
     21: "test22 & long",
     22: "test23",
     23: "test24 < long",
     24: "test25",
     25: "test26 > long",
     26: "test27",
     27: "test28 long",
     28: "test29",
     29: "test30 long"}

TEST_SAMPLES = \
    [{"query": "this is some query", "answer": "this is an answer", "relevant": "yes"}]


class TestSampleGenerator:
    def test_training_percent_invalid(self, tmp_path):
        """Tests whether the class handles invalid training set percentage values gracefully."""
        dg = SampleGenerator(MOCK_DATASET, 30)

        # Negative
        with pytest.raises(ValueError) as error:
            dg.generate_samples(tmp_path,
                                train_sample_ratio=-1)
        assert "[0, 1]" in str(error.value)

        # Above 100
        with pytest.raises(ValueError) as error:
            dg.generate_samples(tmp_path,
                                train_sample_ratio=1.01)
        assert "[0, 1]" in str(error.value)

    def test_negative_ratio_invalid(self, tmp_path):
        """Tests whether the class handles invalid negative ratios values gracefully."""
        dg = SampleGenerator(MOCK_DATASET, 30)

        # Negative
        with pytest.raises(ValueError) as error:
            dg.generate_samples(tmp_path,
                                neg_ratio=-1)
        assert "non-negative" in str(error.value)

    def test_max_chars_invalid(self, tmp_path):
        """Tests whether the class handles invalid character limit values gracefully."""
        dg = SampleGenerator(MOCK_DATASET, 30)

        # Negative
        with pytest.raises(ValueError) as error:
            dg.generate_samples(tmp_path,
                                max_chars=-1)
        assert "non-negative" in str(error.value)

    def test_positive_samples_default(self, tmp_path):
        """Tests whether the class generates correct positive samples with default parameters."""
        dg = SampleGenerator(MOCK_DATASET, 30)
        dg.generate_samples(tmp_path)

        # Iterate over comments
        for comment in range(30):
            # Assert that each child comment has a positive sample
            for child in CORRECT_CHILD_IDS[comment]:
                correct_positive = {"query": COMMENT_BODY[comment], "answer": COMMENT_BODY[child],
                                    "relevant": "yes"}
                assert correct_positive in dg.positive_samples

            # Assert that each non-child comment does not have a positive sample
            non_child_ids = [i for i in range(30) if i not in CORRECT_CHILD_IDS[comment]]
            for non_child in non_child_ids:
                incorrect_positive = {"query": COMMENT_BODY[comment], "answer": COMMENT_BODY[non_child],
                                      "relevant": "yes"}
                assert incorrect_positive not in dg.positive_samples

    def test_negative_samples_default(self, tmp_path):
        """Tests whether the class generates correct negative samples with default parameters."""
        dg = SampleGenerator(MOCK_DATASET, 30)
        dg.generate_samples(tmp_path)

        # Assert that each negative answer comes from an actual comment
        for negative in dg.negative_samples:
            assert negative["answer"] in COMMENT_BODY.values()

        # Iterate over comments
        for comment in range(30):
            # Assert that each child comment does not have a negative sample
            for child_id in CORRECT_CHILD_IDS[comment]:
                incorrect_negative = {"query": COMMENT_BODY[comment], "answer": COMMENT_BODY[child_id],
                                      "relevant": "no"}
                assert incorrect_negative not in dg.negative_samples

    def test_positive_samples_num_lines_specified_5(self, tmp_path):
        """Tests whether the class generates correct positive samples when the line count is set to 5."""
        dg = SampleGenerator(MOCK_DATASET, 5)
        dg.generate_samples(tmp_path)

        # Iterate over in-range comments
        for comment in range(5):
            # Iterate over in-range child comments, and assert that each has a positive sample
            in_range_child_ids = [i for i in CORRECT_CHILD_IDS[comment] if i < 5]
            for in_range_child in in_range_child_ids:
                correct_positive = {"query": COMMENT_BODY[comment],
                                    "answer": COMMENT_BODY[in_range_child],
                                    "relevant": "yes"}
                assert correct_positive in dg.positive_samples

            # Iterate over out-of-range child comments, and assert that none have a positive sample
            out_of_range_child_ids = [i for i in CORRECT_CHILD_IDS[comment] if i >= 5]
            for out_of_range_child in out_of_range_child_ids:
                incorrect_positive = {"query": COMMENT_BODY[comment],
                                      "answer": COMMENT_BODY[out_of_range_child],
                                      "relevant": "yes"}
                assert incorrect_positive not in dg.positive_samples

            # Iterate over non-child comments, and assert that none have a positive sample
            non_child_ids = [i for i in range(30) if i not in CORRECT_CHILD_IDS[comment]]
            for non_child in non_child_ids:
                incorrect_positive = {"query": COMMENT_BODY[comment], "answer": COMMENT_BODY[non_child],
                                      "relevant": "yes"}
                assert incorrect_positive not in dg.positive_samples

        # Iterate over out-of-range comments, and assert that none are the query of an sample
        for comment in range(5, 30):
            for sample in dg.positive_samples:
                assert not sample["query"] == COMMENT_BODY[comment]

    def test_positive_samples_num_lines_specified_20(self, tmp_path):
        """Tests whether the class generates correct positive samples when the line count is set to 20."""
        dg = SampleGenerator(MOCK_DATASET, 20)
        dg.generate_samples(tmp_path)

        # Iterate over in-range comments
        for comment in range(20):
            # Iterate over in-range child comments, and assert that each has a positive sample
            in_range_child_ids = [i for i in CORRECT_CHILD_IDS[comment] if i < 20]
            for in_range_child in in_range_child_ids:
                correct_positive = {"query": COMMENT_BODY[comment],
                                    "answer": COMMENT_BODY[in_range_child],
                                    "relevant": "yes"}
                assert correct_positive in dg.positive_samples

            # Iterate over out-of-range child comments, and assert that none have a positive sample
            out_of_range_child_ids = [i for i in CORRECT_CHILD_IDS[comment] if i >= 20]
            for out_of_range_child in out_of_range_child_ids:
                incorrect_positive = {"query": COMMENT_BODY[comment],
                                      "answer": COMMENT_BODY[out_of_range_child],
                                      "relevant": "yes"}
                assert incorrect_positive not in dg.positive_samples

            # Iterate over non-child comments, and assert that none have a positive sample
            non_child_ids = [i for i in range(30) if i not in CORRECT_CHILD_IDS[comment]]
            for non_child in non_child_ids:
                incorrect_positive = {"query": COMMENT_BODY[comment], "answer": COMMENT_BODY[non_child],
                                      "relevant": "yes"}
                assert incorrect_positive not in dg.positive_samples

        # Iterate over out-of-range comments, and assert that none are the query of an sample
        for comment in range(20, 30):
            for sample in dg.positive_samples:
                assert not sample["query"] == COMMENT_BODY[comment]

    def test_negative_samples_num_lines_specified_5(self, tmp_path):
        """Tests whether the class generates correct negative samples when the line count is set to 5."""
        dg = SampleGenerator(MOCK_DATASET, 5)
        dg.generate_samples(tmp_path)

        # Assert that each negative sample comes from an actual comment
        for sample in dg.negative_samples:
            assert sample["answer"] in COMMENT_BODY.values()

        # Iterate over in-range comments
        for comment in range(5):
            # Iterate over out of range non-children, and assert that none have a negative sample
            out_of_range_non_child_ids = [i for i in range(30) if
                                          i not in CORRECT_CHILD_IDS[comment] and i >= 5]
            for out_of_range_non_child in out_of_range_non_child_ids:
                incorrect_negative = {"query": COMMENT_BODY[comment],
                                      "answer": COMMENT_BODY[out_of_range_non_child],
                                      "relevant": "no"}
                assert incorrect_negative not in dg.negative_samples

            # Iterate over children, and assert that none have a negative sample
            for child in CORRECT_CHILD_IDS[comment]:
                incorrect_negative = {"query": COMMENT_BODY[comment], "answer": COMMENT_BODY[child],
                                      "relevant": "no"}
                assert incorrect_negative not in dg.negative_samples

        # Iterate over out-of-range comments, and assert that none are the query of an sample
        for comment in range(5, 30):
            for sample in dg.negative_samples:
                assert not sample["query"] == COMMENT_BODY[comment]

    def test_negative_samples_num_lines_specified_20(self, tmp_path):
        """Tests whether the class generates correct negative samples when the line count is set to 20."""
        dg = SampleGenerator(MOCK_DATASET, 20)
        dg.generate_samples(tmp_path)

        # Assert that each negative sample comes from an actual comment
        for sample in dg.negative_samples:
            assert sample["answer"] in COMMENT_BODY.values()

        # Iterate over in-range comments
        for comment in range(20):
            # Iterate over out of range non-children, and assert that none have a negative sample
            out_of_range_non_child_ids = [i for i in range(30) if
                                          i not in CORRECT_CHILD_IDS[comment] and i >= 20]
            for out_of_range_non_child in out_of_range_non_child_ids:
                incorrect_negative = {"query": COMMENT_BODY[comment],
                                      "answer": COMMENT_BODY[out_of_range_non_child],
                                      "relevant": "no"}
                assert incorrect_negative not in dg.negative_samples

            # Iterate over children, and assert that none have a negative sample
            for child in CORRECT_CHILD_IDS[comment]:
                incorrect_negative = {"query": COMMENT_BODY[comment], "answer": COMMENT_BODY[child],
                                      "relevant": "no"}
                assert incorrect_negative not in dg.negative_samples

        # Iterate over out-of-range comments, and assert that none are the query of an sample
        for comment in range(20, 30):
            for sample in dg.negative_samples:
                assert not sample["query"] == COMMENT_BODY[comment]

    def test_negative_ratio_default(self, tmp_path):
        """Tests whether the class generates the correct number of negatives (1 per positive) by default."""
        dg = SampleGenerator(MOCK_DATASET, 30)
        dg.generate_samples(tmp_path)

        # Assert that each negative count equals the corresponding positive count
        for id in dg.comment_id_to_child_ids:
            assert dg.num_negatives[id] == dg.num_positives[id]

        # And assert that the length of the negative samples matches this
        assert len(dg.negative_samples) == sum(dg.num_negatives.values())
        assert len(dg.negative_samples) == sum(dg.num_positives.values())
        assert len(dg.negative_samples) == len(dg.positive_samples)

    def test_negative_ratio_specified_0(self, tmp_path):
        """Tests whether the class generates no negatives when the ratio is set to 0."""
        dg = SampleGenerator(MOCK_DATASET, 30)
        dg.generate_samples(tmp_path,
                            neg_ratio=0)

        # Assert that each negative count equals zero
        zero_dict = {x: 0 for x in dg.comment_id_to_child_ids}
        assert dg.num_negatives == zero_dict

        # And check that the length of the negative samples matches this
        assert len(dg.negative_samples) == 0

    def test_negative_ratio_specified_5(self, tmp_path):
        """Tests whether the class generates 5 negatives per positive when the ratio is set to 5."""
        dg = SampleGenerator(MOCK_DATASET, 30)
        dg.generate_samples(tmp_path,
                            neg_ratio=5)

        # Assert that each negative count equals the corresponding positive count multiplied by 5
        for i in dg.comment_id_to_child_ids:
            assert dg.num_negatives[i] == dg.num_positives[i] * 5

        # And assert that the length of the negative samples matches this
        assert len(dg.negative_samples) == sum(dg.num_negatives.values())
        assert len(dg.negative_samples) == sum(dg.num_positives.values()) * 5
        assert len(dg.negative_samples) == len(dg.positive_samples) * 5

    def test_negative_ratio_specified_large(self, tmp_path):
        """Tests whether the class stops generating negatives if it runs out of negatives to sample
        (i.e. when the ratio is larger than the sample size)."""
        dg = SampleGenerator(MOCK_DATASET, 30)
        dg.generate_samples(tmp_path,
                            neg_ratio=31)

        # Assert that each negative count equals the total number of comments minus the number of positives,
        # or that the number of positives is 0
        for i in dg.comment_id_to_child_ids:
            assert dg.num_negatives[i] == 30 - dg.num_positives[i] or dg.num_positives[i] == 0

        # Assert that the length of the negative samples matches this
        assert len(dg.negative_samples) == sum(dg.num_negatives.values())

    def test_positive_samples_max_chars_specified_0(self, tmp_path):
        """Tests whether the class generates no positive samples when max chars is set to 0."""
        dg = SampleGenerator(MOCK_DATASET, 30)
        dg.generate_samples(tmp_path,
                            max_chars=0)

        assert dg.positive_samples == []

    def test_positive_samples_max_chars_specified_5(self, tmp_path):
        """Tests whether the class generates the correct positive samples when max chars is set to 5."""
        dg = SampleGenerator(MOCK_DATASET, 30)
        dg.generate_samples(tmp_path,
                            max_chars=5)

        # Iterate over comments that meet the max char limit
        good_comment_ids = [i for i in range(30) if len(COMMENT_BODY[i]) <= 5]
        for good_comment in good_comment_ids:

            # Iterate over child comments that meet char limit, and assert that each has a positive sample
            good_child_ids = [i for i in CORRECT_CHILD_IDS[good_comment] if len(COMMENT_BODY[i]) <= 5]
            for good_child in good_child_ids:
                correct_positive = {"query": COMMENT_BODY[good_comment], "answer": COMMENT_BODY[good_child],
                                    "relevant": "yes"}
                assert correct_positive in dg.positive_samples

            # Iterate over child comments that do not meet char limit, and assert that none have a positive sample
            bad_child_ids = [i for i in CORRECT_CHILD_IDS[good_comment] if len(COMMENT_BODY[i]) > 5]
            for bad_child in bad_child_ids:
                incorrect_positive = {"query": COMMENT_BODY[good_comment], "answer": COMMENT_BODY[bad_child],
                                      "relevant": "yes"}
                assert incorrect_positive not in dg.positive_samples

            # Iterate over non-child comments, and assert that none have a positive sample
            non_child_ids = [i for i in range(30) if i not in CORRECT_CHILD_IDS[good_comment]]
            for non_child in non_child_ids:
                incorrect_positive = {"query": COMMENT_BODY[good_comment], "answer": COMMENT_BODY[non_child],
                                      "relevant": "yes"}
                assert incorrect_positive not in dg.positive_samples

        # Iterate over comments that do not meet the max char limit, and assert that none are the query of an sample
        bad_comment_ids = [i for i in range(30) if len(COMMENT_BODY[i]) > 5]
        for bad_comment in bad_comment_ids:
            for sample in dg.positive_samples:
                assert not sample["query"] == bad_comment

    def test_positive_samples_max_chars_specified_10(self, tmp_path):
        """Tests whether the class generates the correct positive samples when max chars is set to 10."""
        dg = SampleGenerator(MOCK_DATASET, 30)
        dg.generate_samples(tmp_path,
                            max_chars=10)

        # Iterate over comments that meet the max char limit
        good_comment_ids = [i for i in range(30) if len(COMMENT_BODY[i]) <= 10]
        for good_comment in good_comment_ids:
            # Iterate over child comments that meet char limit, and assert that each has a positive sample
            good_child_ids = [i for i in CORRECT_CHILD_IDS[good_comment] if len(COMMENT_BODY[i]) <= 10]
            for good_child in good_child_ids:
                correct_positive = {"query": COMMENT_BODY[good_comment], "answer": COMMENT_BODY[good_child],
                                    "relevant": "yes"}
                assert correct_positive in dg.positive_samples

            # Make sure child comments that do not meet char limit have no positive samples
            bad_child_ids = [i for i in CORRECT_CHILD_IDS[good_comment] if len(COMMENT_BODY[i]) > 10]
            for bad_child in bad_child_ids:
                incorrect_positive = {"query": COMMENT_BODY[good_comment], "answer": COMMENT_BODY[bad_child],
                                      "relevant": "yes"}
                assert incorrect_positive not in dg.positive_samples

            # Iterate over non-child comments, and assert that none have a positive sample
            non_child_ids = [i for i in range(30) if i not in CORRECT_CHILD_IDS[good_comment]]
            for non_child in non_child_ids:
                incorrect_positive = {"query": COMMENT_BODY[good_comment], "answer": COMMENT_BODY[non_child],
                                      "relevant": "yes"}
                assert incorrect_positive not in dg.positive_samples

        # Iterate over comments that do not meet the max char limit, and assert that none are the query of an sample
        bad_comment_ids = [i for i in range(30) if len(COMMENT_BODY[i]) > 10]
        for bad_comment in bad_comment_ids:
            for sample in dg.positive_samples:
                assert not sample["query"] == bad_comment

    def test_negative_samples_max_chars_specified_0(self, tmp_path):
        """Tests whether the class generates no negative samples when max chars is set to 0."""
        dg = SampleGenerator(MOCK_DATASET, 30)
        dg.generate_samples(tmp_path,
                            max_chars=0)

        assert dg.negative_samples == []

    def test_negative_samples_max_chars_specified_5(self, tmp_path):
        """Tests whether the class generates correct negative samples when max chars is set to 5."""
        dg = SampleGenerator(MOCK_DATASET, 30)
        dg.generate_samples(tmp_path,
                            max_chars=5)

        # Assert that each negative sample comes from an actual comment
        for sample in dg.negative_samples:
            assert sample["answer"] in COMMENT_BODY.values()

        # Iterate over comments that meet the max char limit
        good_comment_ids = [i for i in range(30) if len(COMMENT_BODY[i]) <= 5]
        for good_comment in good_comment_ids:
            # Iterate over non-children that do not meet the char limit, and assert that none have a negative sample
            bad_non_child_ids = [i for i in range(30) if i not in CORRECT_CHILD_IDS[good_comment]
                                 and len(COMMENT_BODY[i]) > 5]
            for bad_non_child in bad_non_child_ids:
                incorrect_negative = {"query": COMMENT_BODY[good_comment],
                                      "answer": COMMENT_BODY[bad_non_child],
                                      "relevant": "no"}
                assert incorrect_negative not in dg.negative_samples

            # Iterate over children, and assert that none have a negative sample
            for child in CORRECT_CHILD_IDS[good_comment]:
                incorrect_negative = {"query": COMMENT_BODY[good_comment], "answer": COMMENT_BODY[child],
                                      "relevant": "no"}
                assert incorrect_negative not in dg.negative_samples

        # Iterate over comments that do not meet the max char limit, and assert that none are the query of an sample
        bad_comment_ids = [i for i in range(30) if len(COMMENT_BODY[i]) > 5]
        for bad_comment in bad_comment_ids:
            for sample in dg.negative_samples:
                assert not sample["query"] == COMMENT_BODY[bad_comment]

    def test_negative_samples_max_chars_specified_10(self, tmp_path):
        """Tests whether the class generates correct negative samples when max chars is set to 10."""
        dg = SampleGenerator(MOCK_DATASET, 30)
        dg.generate_samples(tmp_path,
                            max_chars=10)

        # Assert that each negative sample comes from an actual comment
        for sample in dg.negative_samples:
            assert sample["answer"] in COMMENT_BODY.values()

        # Iterate over comments that meet the max char limit
        good_comment_ids = [i for i in range(30) if len(COMMENT_BODY[i]) <= 10]
        for good_comment in good_comment_ids:
            # Iterate over non-children that do not meet the char limit, and assert that none have a negative sample
            bad_non_child_ids = [i for i in range(30) if i not in CORRECT_CHILD_IDS[good_comment]
                                 and len(COMMENT_BODY[i]) > 10]
            for bad_non_child in bad_non_child_ids:
                incorrect_negative = {"query": COMMENT_BODY[good_comment],
                                      "answer": COMMENT_BODY[bad_non_child],
                                      "relevant": "no"}
                assert incorrect_negative not in dg.negative_samples

            # Iterate over children, and assert that none have a negative sample
            for child in CORRECT_CHILD_IDS[good_comment]:
                incorrect_negative = {"query": COMMENT_BODY[good_comment], "answer": COMMENT_BODY[child],
                                      "relevant": "no"}
                assert incorrect_negative not in dg.negative_samples

        # Iterate over comments that do not meet the max char limit, and assert that none are the query of an sample
        bad_comment_ids = [i for i in range(30) if len(COMMENT_BODY[i]) > 10]
        for bad_comment in bad_comment_ids:
            for sample in dg.negative_samples:
                assert not sample["query"] == COMMENT_BODY[bad_comment]

    def test_training_ratio_specified_0(self, tmp_path):
        """Tests whether the class generates no training samples and all test samples when the training percent
        is set to 0."""
        dg = SampleGenerator(MOCK_DATASET, 30)
        dg.generate_samples(tmp_path,
                            train_sample_ratio=0)

        total_samples = len(dg.positive_samples) + len(dg.negative_samples)

        assert len(dg.train_samples) == 0
        assert len(dg.eval_samples) + len(dg.dev_samples) == total_samples

    def test_training_ratio_specified_25(self, tmp_path):
        """Tests whether the class generates the correct number of training and test samples when the training ratio
        is set to 0.25."""
        dg = SampleGenerator(MOCK_DATASET, 30)
        dg.generate_samples(tmp_path,
                            train_sample_ratio=0.25)

        total_samples = len(dg.positive_samples) + len(dg.negative_samples)

        assert len(dg.train_samples) == math.floor(total_samples * 0.25)
        assert len(dg.dev_samples) + len(dg.eval_samples) == math.ceil(total_samples * 0.75)

    def test_training_ratio_specified_75(self, tmp_path):
        """Tests whether the class generates the correct number of training and test samples when the training ratio
        is set to 0.75."""
        dg = SampleGenerator(MOCK_DATASET, 30)
        dg.generate_samples(tmp_path,
                            train_sample_ratio=0.75)

        total_samples = len(dg.positive_samples) + len(dg.negative_samples)

        assert len(dg.train_samples) == math.floor(total_samples * 0.75)
        assert len(dg.dev_samples) + len(dg.eval_samples) == math.ceil(total_samples * 0.25)

    def test_training_ratio_specified_100(self, tmp_path):
        """Tests whether the class generates all training samples and no test samples when the training percent
        is set to 100."""
        dg = SampleGenerator(MOCK_DATASET, 30)
        dg.generate_samples(tmp_path,
                            train_sample_ratio=1)

        total_samples = len(dg.positive_samples) + len(dg.negative_samples)

        assert len(dg.train_samples) == total_samples
        assert len(dg.dev_samples) == 0
        assert len(dg.eval_samples) == 0

    def test_seed_default(self, tmp_path):
        """Tests that, by default, a random seed is used, generating different training and test sets."""
        dg_1 = SampleGenerator(MOCK_DATASET, 30)
        dg_1.generate_samples(tmp_path)

        dg_2 = SampleGenerator(MOCK_DATASET, 30)
        dg_2.generate_samples(tmp_path)

        assert not dg_1.train_samples == dg_2.train_samples
        assert not dg_1.eval_samples == dg_2.eval_samples

    def test_seed_specified_same(self, tmp_path):
        """Tests that the same training/test sets are generated when using the same random seed twice."""
        dg_1 = SampleGenerator(MOCK_DATASET, 30)
        dg_1.generate_samples(tmp_path,
                              seed=123)

        dg_2 = SampleGenerator(MOCK_DATASET, 30)
        dg_2.generate_samples(tmp_path,
                              seed=123)

        assert dg_1.train_samples == dg_2.train_samples
        assert dg_1.eval_samples == dg_2.eval_samples

    def test_seed_specified_different(self, tmp_path):
        """Tests that different training/test sets are generated when using different seeds."""
        dg_1 = SampleGenerator(MOCK_DATASET, 30)
        dg_1.generate_samples(tmp_path,
                              seed=123)

        dg_2 = SampleGenerator(MOCK_DATASET, 30)
        dg_2.generate_samples(tmp_path,
                              seed=456)

        assert not dg_1.train_samples == dg_2.train_samples
        assert not dg_1.eval_samples == dg_2.eval_samples

    def test_file_format_invalid(self):
        """Tests whether the class handles invalid output file types gracefully"""
        dg = SampleGenerator(MOCK_DATASET, 30)

        with pytest.raises(ValueError) as error:
            dg.write_samples_to_file(samples=[], filepath="test.out", fmt="invalid")
        assert "not supported" in str(error.value)

    def test_json_output(self, tmp_path):
        """Tests that dicts are written out in json format correctly."""
        dg = SampleGenerator(MOCK_DATASET, 30)
        dg.write_samples_to_file(TEST_SAMPLES, tmp_path / "json_test.json", fmt="json")

        expected = '{"query":"this is some query","answer":"this is an answer","relevant":"yes"}\n'
        with open(tmp_path / "json_test.json", "r") as file:
            actual = file.read()
            assert actual == expected

    def test_csv_output(self, tmp_path):
        """Tests that dicts are written out in csv format correctly."""
        dg = SampleGenerator(MOCK_DATASET, 30)
        dg.write_samples_to_file(TEST_SAMPLES, tmp_path / "csv_test.csv", fmt="csv")

        expected = 'this is some query,this is an answer,yes\n'
        with open(tmp_path / "csv_test.csv", "r") as file:
            actual = file.read()
            assert actual == expected

    def test_tsv_output(self, tmp_path):
        """Tests that dicts are written out in tsv format correctly."""
        dg = SampleGenerator(MOCK_DATASET, 30)
        dg.write_samples_to_file(TEST_SAMPLES, tmp_path / "tsv_test.tsv", fmt="tsv")

        expected = 'this is some query\tthis is an answer\tyes\n'

        with open(tmp_path / "tsv_test.tsv", "r") as file:
            actual = file.read()
            assert actual == expected
