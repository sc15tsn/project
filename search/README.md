# Search

You must first import the data into Elasticsearch. 
To do that put the `answers.json` file with columns `text` and `sentiment` 
into `search` directory, then use `search/import.py`. 
