"""(Re)creates the Elasticsearch index, and imports the deduplicated dataset into Elasticsearch index.

This script is launched outside of docker.
"""
import argparse
import os
from typing import List

import elasticsearch
from elasticsearch.helpers import parallel_bulk
import rapidjson

INDEX_CREATION_BODY = '''
{  
  "mappings":{  
    "answer":{  
      "properties":{  
        "text":{  
          "type":"text",
        },
        "sentiment":{
          "type":"float",
        },
      }
    }
  }
}'''


def import_data(json_answers: List[str]):
    num_total = len(json_answers)

    print(f"Importing {num_total} answers...")

    settings = {
        "_index": "answer",
        "_type": "_doc",
    }

    num_processed = 0
    for json_answer in json_answers:
        answer = rapidjson.loads(json_answer)
        answer['text'] = answer['text'].strip()

        # Send the answer to Elasticsearch
        yield {**settings, **answer}

        # Print progress
        num_processed += 1
        if num_processed % 1000 == 0:
            print(f"{num_processed}/{num_total} ({100 * num_processed / num_total:.2f}%)")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="(Re)create the elasticsearch index and import data into it.")
    parser.add_argument("input_file", type=str, default='answers.json', help="path to the JSON input file containing answers with "
                                                     "'text' and 'sentiment' keys (default: ./answers.json)")
    parser.add_argument("-n_processes", type=int, default=os.cpu_count(),
                        help="number of processes to use (default: number of cores on the CPU)")
    parser.add_argument("-host", type=str, default='localhost',
                        help="elasticsearch host (default: localhost)")
    args = parser.parse_args()

    print(f"Input file: {args.input_file}")
    print(f"Host: {args.host}")
    print(f"Number of processes: {args.n_processes}")

    # Read the input file
    print("Reading the input file...")
    with open(args.input_file, 'r', encoding='utf-8') as f:
        lines = [l.strip() for l in f.readlines()]
    num_lines_read = len(lines)
    print(f"Read {num_lines_read} lines.")

    # Deduplicate
    print("Deduplicating...")
    lines = list(set(lines))
    print(f"Removed {num_lines_read - len(lines)} duplicates.")

    es = elasticsearch.Elasticsearch([{"host": args.host, "port": 9200}])

    # (Re)create the index
    print("(Re)creating the index...")
    es.indices.delete(index='answer', ignore=[400, 404])
    es.indices.create(index='answer', ignore=400, body=INDEX_CREATION_BODY)

    # Import the data
    for success, info in parallel_bulk(es, import_data(lines), args.n_processes):
        if not success:
            print('An insert failed:', info)

    print("Done.")