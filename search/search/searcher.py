"""Contains searcher class"""
import re
from typing import List

import elasticsearch

from search import log
from search.constants import DEFAULT_MAX_ANSWERS, ES_ANSWER_INDEX, FUZZY_SENTIMENT_NEGATIVE, \
    FUZZY_SENTIMENT_NEUTRAL, FUZZY_SENTIMENT_POSITIVE, SENTIMENT_NEGATIVE_THRESHOLD, \
    SENTIMENT_NEGATIVE_NEUTRAL_THRESHOLD, SENTIMENT_POSITIVE_NEUTRAL_THRESHOLD, SENTIMENT_POSITIVE_THRESHOLD
from search import utils

LOGGER = log.get_logger(__name__)

SENTIMENT_FUZZY_TO_RANGE = {
    FUZZY_SENTIMENT_POSITIVE: (SENTIMENT_POSITIVE_NEUTRAL_THRESHOLD, 1),
    FUZZY_SENTIMENT_NEUTRAL: (SENTIMENT_NEGATIVE_THRESHOLD, SENTIMENT_POSITIVE_THRESHOLD),
    FUZZY_SENTIMENT_NEGATIVE: (-1, SENTIMENT_NEGATIVE_NEUTRAL_THRESHOLD),
    None: (-1, 1)
}


class Searcher:
    """Class for searching for potential answers according to the user provider query.

    Attributes:
        es_connection (elasticsearch.Elasticsearch): Elasticsearch connection
    """

    def __init__(self) -> None:

        self.es_connection = elasticsearch.Elasticsearch([{"host": "elasticsearch", "port": 9200}])
        LOGGER.info("Searcher is ready.")

    def search(self, query: str,
               fuzzy_sentiment: str = None,
               search_text_fuzziness: int = None,
               max_answers: int = DEFAULT_MAX_ANSWERS) -> List[str]:
        """Search for potential answers to a query using Elasticsearch.

        Args:
            query: Query provided by the conversation partner.
            fuzzy_sentiment: Fuzzy sentiment provided by the user (values can be found in constants.py).
            search_text_fuzziness: Fuzziness applied in Elasticsearch for the text.
            max_answers: Maximum number of answers.

        Returns:
            Potential answers.
        """
        assert fuzzy_sentiment in (
            FUZZY_SENTIMENT_POSITIVE, FUZZY_SENTIMENT_NEUTRAL, FUZZY_SENTIMENT_NEGATIVE, None)

        assert max_answers is not None

        # Get the sentiment
        sentiment_range = SENTIMENT_FUZZY_TO_RANGE[fuzzy_sentiment]

        # Update the query to be more like an answer to the same query
        search_text = utils.question_to_statement(query)
        search_text = utils.swap_person(search_text)

        es_search_body = {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match": {
                                "text": {
                                    "query": search_text,
                                },
                            },
                        },
                        {
                            "range": {
                                "sentiment": {
                                    "gte": sentiment_range[0],
                                    "lte": sentiment_range[1],
                                }
                            }
                        }
                    ],
                    "must_not": [
                    ],
                    "should": [
                    ],
                    "filter": [
                    ],
                },
            },
            "from": "0",
            "size": max_answers,
        }

        if search_text_fuzziness:
            es_search_body['query']['bool']['must'][0]['match']['text']['fuzziness'] = search_text_fuzziness

        LOGGER.info(f"Searching %s index (%s).", ES_ANSWER_INDEX, str(es_search_body))

        resp = self.es_connection.search(
            index=ES_ANSWER_INDEX,
            body=es_search_body)

        LOGGER.info("The search for '%s' has returned %d hits.", query, len(resp['hits']['hits']))
        LOGGER.info(resp['hits']['hits'])

        return list({hit['_source']['text'] for hit in resp['hits']['hits']})
