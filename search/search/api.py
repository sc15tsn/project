"""HTTP API interface for Searcher"""
from typing import Any

import falcon

from search import searcher, log
from search.constants import HTTP_QUERY_PARAMS_ENABLED, DEFAULT_MAX_ANSWERS

LOGGER = log.get_logger(__name__)


class SearchResource:
    """Resource object for Search API"""

    def __init__(self) -> None:
        self.searcher = searcher.Searcher()

    @staticmethod
    def get_param(param_name: str,
                  req: falcon.Request,
                  required: bool = True,
                  allow_query_param: bool = HTTP_QUERY_PARAMS_ENABLED) -> Any:
        """Get the parameter from either JSON body or HTTP Query params (if enabled)

        Args:
            param_name: Parameter name.
            req: falcon.Request object where the param_name will be retrieved from.
            resp: falcon.Response object.
            required: True if it is required to retrieve the parameter.
                If it is required and parameter is not specified, it will
                change the resp with HTTP_BAD_REQUEST and an appropriate message.
            allow_query_param: True if HTTP Query parameters should be allowed.

        Returns:
            Parameter if found in the request, otherwise None.
        """
        try:
            # Get the parameter from JSON body
            param = req.media.get(param_name)
        except (AttributeError, falcon.errors.HTTPBadRequest, falcon.errors.HTTPUnsupportedMediaType):
            # Get the parameter from HTTP Query if not found in JSON body
            param = req.get_param(param_name) if allow_query_param else None

        if param is None and required:
            raise falcon.HTTPBadRequest(description="You have to specify '%s' parameter." % param_name)

        return param

    def on_any(self, req: falcon.Request, resp: falcon.Response) -> None:
        """Calls the search method.

        Args:
            req: falcon.Request with `query` parameter specified.
            resp: falcon.Response

        Notes:
            if successful the response body body contains `answers`.
        """
        query = self.get_param('query', req, required=True)
        sentiment = self.get_param('sentiment', req, required=False)
        max_answers = self.get_param('max_answers', req, required=False)
        search_text_fuzziness = self.get_param('search_text_fuzziness', req, required=False)

        max_answers = int(max_answers) if max_answers else DEFAULT_MAX_ANSWERS
        search_text_fuzziness = int(search_text_fuzziness) if search_text_fuzziness else None

        answers = self.searcher.search(query,
                                       sentiment,
                                       max_answers=max_answers,
                                       search_text_fuzziness=search_text_fuzziness)

        resp.media = {'answers': answers}

    def on_get(self, req: falcon.Request, resp: falcon.Response) -> None:
        """Redirects to on_all"""
        self.on_any(req, resp)

    def on_post(self, req: falcon.Request, resp: falcon.Response) -> None:
        """Redirects to on_all"""
        self.on_any(req, resp)


def create() -> falcon.API:
    """Initialises the API.

    Returns: falcon.API
    """
    api = falcon.API()
    search_res = SearchResource()
    api.add_route('/', search_res)

    LOGGER.info('Search API is ready.')

    return api


SEARCH_API = create()
