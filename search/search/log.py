"""Module for creating standardised loggers"""
import logging
import sys

import colorlog

from search.constants import LOGGING_LEVEL

CONSOLE_FMT = colorlog.ColoredFormatter(
    "[%(asctime)s] [%(bold)s%(name)s%(reset)s] [%(log_color)s%(levelname)s%(reset)s] %(message)s"
)


def get_console_handler():
    """Gets console handler with formatter for stdout"""
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(CONSOLE_FMT)

    return console_handler


def get_logger(name):
    """Sets up and gets logger object"""
    logger = logging.getLogger(name)
    logger.setLevel(LOGGING_LEVEL)
    logger.addHandler(get_console_handler())
    logger.propagate = False

    return logger
