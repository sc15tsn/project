import re
from typing import Tuple, List
import nltk

FIRST_SECOND_PERSON_SWAP_MAP = [
    ('i am', 'you are'),
    ('am I', 'are you'),
    ('you are', 'i am'),
    ('are you', 'am i'),
    ('i', 'you'),
    ('me', 'you'),
    ('my', "your"),
    ('mine', "your's"),
    ("myself", "yourself"),
    ('you', 'i'),
    ('your', 'my'),
    ("your's", "mine"),
    ("yourself", "myself"),
]


def swap_person(text: str) -> str:
    """Swap first and second persons in the text.

    Args:
        text: Text that might have words indicating
            first or second person.
    Returns:
        text with first and second person swapped.
    """

    text = text.lower()

    # Replace to unique tokens so that they are not replaced back in the later steps
    for swap in FIRST_SECOND_PERSON_SWAP_MAP:
        # E.g. are you -> 'amXiXREPL'
        text = re.sub(rf'\b{swap[0]}\b', f"{'X'.join(swap[1].split())}XREPL", text)

    # Replace the unique tokens to what they were supposed to represent.
    for swap in FIRST_SECOND_PERSON_SWAP_MAP:
        # E.g. 'amXiXREPL' -> 'am i'.
        text = re.sub(rf"\b{'X'.join(swap[1].split())}XREPL\b", swap[1], text)

    return text


def _get_tok_pos(pos_tagged_text: List[Tuple[str, str]], token_index: int) -> Tuple[str, str]:
    """Get token and POS tag at the

    Args:
        pos_tagged_text: POS tagged text [(token, pos),...].
        token_index: Token index.

    Returns:

    """
    try:
        token, pos = pos_tagged_text[token_index]
    except IndexError:
        token, pos = None, None

    return token, pos


def pos_tag(text: str) -> List[Tuple[str, str]]:
    """POS tag text.

    Combine noun (NN*) and their possessive ending (POS) into one token (NN$)

    Args:
       text: Text to POS tag

    Returns:
        POS tag updated text
    """

    pos_tagged_text = nltk.pos_tag(nltk.word_tokenize(text.lower()))
    new_pos_tagged_text = []

    for idx in range(len(pos_tagged_text)):
        ct, cp = _get_tok_pos(pos_tagged_text, idx)
        nt, np = _get_tok_pos(pos_tagged_text, idx + 1)

        # Force 'i' to tag as PRP.
        if ct == 'i':
            cp = 'PRP'

        # Force here, there to tag as 'IN' if they are 'RB'.
        if ct in ("here", "there") and cp == 'RB':
            cp = 'IN'

        # Combine possessive nouns into one token instead of 2
        if np == 'POS':
            new_pos_tagged_text.append((ct + nt, 'NN$'))
            continue
        if cp == 'POS':
            continue

        new_pos_tagged_text.append((ct, cp))

    return new_pos_tagged_text


def question_to_statement(text: str) -> str:
    """Transform question into a vague statement.

    Args:
        text: Text that might have a question in it.

    Returns:
        text where the question has been transformed into a vague statement.
    """
    pos_tagged_text = pos_tag(text)

    new_pos_tagged_text = []

    skip = 0
    for idx in range(len(pos_tagged_text)):

        # For skipping tokens when they are already handled.
        if skip > 0:
            skip -= 1
            continue

        prev_token, prev_pos = _get_tok_pos(pos_tagged_text, idx - 1)
        token, pos = _get_tok_pos(pos_tagged_text, idx)

        # Question marks to full stops.
        if token == '?':
            new_pos_tagged_text.append(('.', '.'))
            continue

        # Get rid of W words (e.g. where, why), interjections.
        if pos in ('WRB', 'WP', 'UH'):
            continue

        # Swap chunks of text (e.g. "does my really good test work" -> "my really good test does work")
        if pos in ('VB', 'VBP', 'VBZ', 'VBN', 'VBD', 'VBG', 'MD') \
                and prev_pos not in ('PRP', 'NN', 'NNS', 'NNP', 'NNPS', 'DT', 'EX', 'MD', 'RB', 'IN', 'TO'):

            while True:
                x_tok, x_pos = _get_tok_pos(pos_tagged_text, idx + 1 + skip)

                if token is not None:
                    new_pos_tagged_text.append((x_tok, x_pos))

                skip += 1
                if x_pos in (None, 'PRP', 'NN', 'NNS', 'NNP', 'NNPS', 'EX', 'IN') and \
                        _get_tok_pos(pos_tagged_text, idx + 1 + skip)[1] not in ("CC",):
                    break

        # Add the current word
        if token is not None:
            new_pos_tagged_text.append((token, pos))

    return ' '.join([token for token, pos in new_pos_tagged_text if token])
